<?php namespace Tests\Repositories;

use App\Models\Accommodationtype;
use App\Repositories\AccommodationtypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use $NAMESPACE_TEST_TRAITS$\MakeAccommodationtypeTrait;
use Tests\ApiTestTrait;

class AccommodationtypeRepositoryTest extends TestCase
{
    use MakeAccommodationtypeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var AccommodationtypeRepository
     */
    protected $accommodationtypeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->accommodationtypeRepo = \App::make(AccommodationtypeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_accommodationtype()
    {
        $accommodationtype = $this->fakeAccommodationtypeData();
        $createdAccommodationtype = $this->accommodationtypeRepo->create($accommodationtype);
        $createdAccommodationtype = $createdAccommodationtype->toArray();
        $this->assertArrayHasKey('id', $createdAccommodationtype);
        $this->assertNotNull($createdAccommodationtype['id'], 'Created Accommodationtype must have id specified');
        $this->assertNotNull(Accommodationtype::find($createdAccommodationtype['id']), 'Accommodationtype with given id must be in DB');
        $this->assertModelData($accommodationtype, $createdAccommodationtype);
    }

    /**
     * @test read
     */
    public function test_read_accommodationtype()
    {
        $accommodationtype = $this->makeAccommodationtype();
        $dbAccommodationtype = $this->accommodationtypeRepo->find($accommodationtype->id);
        $dbAccommodationtype = $dbAccommodationtype->toArray();
        $this->assertModelData($accommodationtype->toArray(), $dbAccommodationtype);
    }

    /**
     * @test update
     */
    public function test_update_accommodationtype()
    {
        $accommodationtype = $this->makeAccommodationtype();
        $fakeAccommodationtype = $this->fakeAccommodationtypeData();
        $updatedAccommodationtype = $this->accommodationtypeRepo->update($fakeAccommodationtype, $accommodationtype->id);
        $this->assertModelData($fakeAccommodationtype, $updatedAccommodationtype->toArray());
        $dbAccommodationtype = $this->accommodationtypeRepo->find($accommodationtype->id);
        $this->assertModelData($fakeAccommodationtype, $dbAccommodationtype->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_accommodationtype()
    {
        $accommodationtype = $this->makeAccommodationtype();
        $resp = $this->accommodationtypeRepo->delete($accommodationtype->id);
        $this->assertTrue($resp);
        $this->assertNull(Accommodationtype::find($accommodationtype->id), 'Accommodationtype should not exist in DB');
    }
}
