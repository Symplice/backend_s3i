<?php namespace Tests\Repositories;

use App\Models\Albumphoto;
use App\Repositories\AlbumphotoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use $NAMESPACE_TEST_TRAITS$\MakeAlbumphotoTrait;
use Tests\ApiTestTrait;

class AlbumphotoRepositoryTest extends TestCase
{
    use MakeAlbumphotoTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var AlbumphotoRepository
     */
    protected $albumphotoRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->albumphotoRepo = \App::make(AlbumphotoRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_albumphoto()
    {
        $albumphoto = $this->fakeAlbumphotoData();
        $createdAlbumphoto = $this->albumphotoRepo->create($albumphoto);
        $createdAlbumphoto = $createdAlbumphoto->toArray();
        $this->assertArrayHasKey('id', $createdAlbumphoto);
        $this->assertNotNull($createdAlbumphoto['id'], 'Created Albumphoto must have id specified');
        $this->assertNotNull(Albumphoto::find($createdAlbumphoto['id']), 'Albumphoto with given id must be in DB');
        $this->assertModelData($albumphoto, $createdAlbumphoto);
    }

    /**
     * @test read
     */
    public function test_read_albumphoto()
    {
        $albumphoto = $this->makeAlbumphoto();
        $dbAlbumphoto = $this->albumphotoRepo->find($albumphoto->id);
        $dbAlbumphoto = $dbAlbumphoto->toArray();
        $this->assertModelData($albumphoto->toArray(), $dbAlbumphoto);
    }

    /**
     * @test update
     */
    public function test_update_albumphoto()
    {
        $albumphoto = $this->makeAlbumphoto();
        $fakeAlbumphoto = $this->fakeAlbumphotoData();
        $updatedAlbumphoto = $this->albumphotoRepo->update($fakeAlbumphoto, $albumphoto->id);
        $this->assertModelData($fakeAlbumphoto, $updatedAlbumphoto->toArray());
        $dbAlbumphoto = $this->albumphotoRepo->find($albumphoto->id);
        $this->assertModelData($fakeAlbumphoto, $dbAlbumphoto->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_albumphoto()
    {
        $albumphoto = $this->makeAlbumphoto();
        $resp = $this->albumphotoRepo->delete($albumphoto->id);
        $this->assertTrue($resp);
        $this->assertNull(Albumphoto::find($albumphoto->id), 'Albumphoto should not exist in DB');
    }
}
