<?php namespace Tests\Repositories;

use App\Models\Subscriptionfolder;
use App\Repositories\SubscriptionfolderRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use $NAMESPACE_TEST_TRAITS$\MakeSubscriptionfolderTrait;
use Tests\ApiTestTrait;

class SubscriptionfolderRepositoryTest extends TestCase
{
    use MakeSubscriptionfolderTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var SubscriptionfolderRepository
     */
    protected $subscriptionfolderRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->subscriptionfolderRepo = \App::make(SubscriptionfolderRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_subscriptionfolder()
    {
        $subscriptionfolder = $this->fakeSubscriptionfolderData();
        $createdSubscriptionfolder = $this->subscriptionfolderRepo->create($subscriptionfolder);
        $createdSubscriptionfolder = $createdSubscriptionfolder->toArray();
        $this->assertArrayHasKey('id', $createdSubscriptionfolder);
        $this->assertNotNull($createdSubscriptionfolder['id'], 'Created Subscriptionfolder must have id specified');
        $this->assertNotNull(Subscriptionfolder::find($createdSubscriptionfolder['id']), 'Subscriptionfolder with given id must be in DB');
        $this->assertModelData($subscriptionfolder, $createdSubscriptionfolder);
    }

    /**
     * @test read
     */
    public function test_read_subscriptionfolder()
    {
        $subscriptionfolder = $this->makeSubscriptionfolder();
        $dbSubscriptionfolder = $this->subscriptionfolderRepo->find($subscriptionfolder->id);
        $dbSubscriptionfolder = $dbSubscriptionfolder->toArray();
        $this->assertModelData($subscriptionfolder->toArray(), $dbSubscriptionfolder);
    }

    /**
     * @test update
     */
    public function test_update_subscriptionfolder()
    {
        $subscriptionfolder = $this->makeSubscriptionfolder();
        $fakeSubscriptionfolder = $this->fakeSubscriptionfolderData();
        $updatedSubscriptionfolder = $this->subscriptionfolderRepo->update($fakeSubscriptionfolder, $subscriptionfolder->id);
        $this->assertModelData($fakeSubscriptionfolder, $updatedSubscriptionfolder->toArray());
        $dbSubscriptionfolder = $this->subscriptionfolderRepo->find($subscriptionfolder->id);
        $this->assertModelData($fakeSubscriptionfolder, $dbSubscriptionfolder->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_subscriptionfolder()
    {
        $subscriptionfolder = $this->makeSubscriptionfolder();
        $resp = $this->subscriptionfolderRepo->delete($subscriptionfolder->id);
        $this->assertTrue($resp);
        $this->assertNull(Subscriptionfolder::find($subscriptionfolder->id), 'Subscriptionfolder should not exist in DB');
    }
}
