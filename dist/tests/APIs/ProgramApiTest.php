<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use $NAMESPACE_TEST_TRAITS$\MakeProgramTrait;
use Tests\ApiTestTrait;

class ProgramApiTest extends TestCase
{
    use MakeProgramTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_program()
    {
        $program = $this->fakeProgramData();
        $this->response = $this->json('POST', '/api/programs', $program);

        $this->assertApiResponse($program);
    }

    /**
     * @test
     */
    public function test_read_program()
    {
        $program = $this->makeProgram();
        $this->response = $this->json('GET', '/api/programs/'.$program->id);

        $this->assertApiResponse($program->toArray());
    }

    /**
     * @test
     */
    public function test_update_program()
    {
        $program = $this->makeProgram();
        $editedProgram = $this->fakeProgramData();

        $this->response = $this->json('PUT', '/api/programs/'.$program->id, $editedProgram);

        $this->assertApiResponse($editedProgram);
    }

    /**
     * @test
     */
    public function test_delete_program()
    {
        $program = $this->makeProgram();
        $this->response = $this->json('DELETE', '/api/programs/'.$program->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/programs/'.$program->id);

        $this->response->assertStatus(404);
    }
}
