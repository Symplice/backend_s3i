<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use $NAMESPACE_TEST_TRAITS$\MakeAlbumphotoTrait;
use Tests\ApiTestTrait;

class AlbumphotoApiTest extends TestCase
{
    use MakeAlbumphotoTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_albumphoto()
    {
        $albumphoto = $this->fakeAlbumphotoData();
        $this->response = $this->json('POST', '/api/albumphotos', $albumphoto);

        $this->assertApiResponse($albumphoto);
    }

    /**
     * @test
     */
    public function test_read_albumphoto()
    {
        $albumphoto = $this->makeAlbumphoto();
        $this->response = $this->json('GET', '/api/albumphotos/'.$albumphoto->id);

        $this->assertApiResponse($albumphoto->toArray());
    }

    /**
     * @test
     */
    public function test_update_albumphoto()
    {
        $albumphoto = $this->makeAlbumphoto();
        $editedAlbumphoto = $this->fakeAlbumphotoData();

        $this->response = $this->json('PUT', '/api/albumphotos/'.$albumphoto->id, $editedAlbumphoto);

        $this->assertApiResponse($editedAlbumphoto);
    }

    /**
     * @test
     */
    public function test_delete_albumphoto()
    {
        $albumphoto = $this->makeAlbumphoto();
        $this->response = $this->json('DELETE', '/api/albumphotos/'.$albumphoto->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/albumphotos/'.$albumphoto->id);

        $this->response->assertStatus(404);
    }
}
