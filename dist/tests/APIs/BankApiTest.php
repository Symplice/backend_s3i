<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use $NAMESPACE_TEST_TRAITS$\MakeBankTrait;
use Tests\ApiTestTrait;

class BankApiTest extends TestCase
{
    use MakeBankTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_bank()
    {
        $bank = $this->fakeBankData();
        $this->response = $this->json('POST', '/api/banks', $bank);

        $this->assertApiResponse($bank);
    }

    /**
     * @test
     */
    public function test_read_bank()
    {
        $bank = $this->makeBank();
        $this->response = $this->json('GET', '/api/banks/'.$bank->id);

        $this->assertApiResponse($bank->toArray());
    }

    /**
     * @test
     */
    public function test_update_bank()
    {
        $bank = $this->makeBank();
        $editedBank = $this->fakeBankData();

        $this->response = $this->json('PUT', '/api/banks/'.$bank->id, $editedBank);

        $this->assertApiResponse($editedBank);
    }

    /**
     * @test
     */
    public function test_delete_bank()
    {
        $bank = $this->makeBank();
        $this->response = $this->json('DELETE', '/api/banks/'.$bank->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/banks/'.$bank->id);

        $this->response->assertStatus(404);
    }
}
