<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use $NAMESPACE_TEST_TRAITS$\MakePaymentTrait;
use Tests\ApiTestTrait;

class PaymentApiTest extends TestCase
{
    use MakePaymentTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_payment()
    {
        $payment = $this->fakePaymentData();
        $this->response = $this->json('POST', '/api/payments', $payment);

        $this->assertApiResponse($payment);
    }

    /**
     * @test
     */
    public function test_read_payment()
    {
        $payment = $this->makePayment();
        $this->response = $this->json('GET', '/api/payments/'.$payment->id);

        $this->assertApiResponse($payment->toArray());
    }

    /**
     * @test
     */
    public function test_update_payment()
    {
        $payment = $this->makePayment();
        $editedPayment = $this->fakePaymentData();

        $this->response = $this->json('PUT', '/api/payments/'.$payment->id, $editedPayment);

        $this->assertApiResponse($editedPayment);
    }

    /**
     * @test
     */
    public function test_delete_payment()
    {
        $payment = $this->makePayment();
        $this->response = $this->json('DELETE', '/api/payments/'.$payment->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/payments/'.$payment->id);

        $this->response->assertStatus(404);
    }
}
