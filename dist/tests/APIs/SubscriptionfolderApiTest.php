<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use $NAMESPACE_TEST_TRAITS$\MakeSubscriptionfolderTrait;
use Tests\ApiTestTrait;

class SubscriptionfolderApiTest extends TestCase
{
    use MakeSubscriptionfolderTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_subscriptionfolder()
    {
        $subscriptionfolder = $this->fakeSubscriptionfolderData();
        $this->response = $this->json('POST', '/api/subscriptionfolders', $subscriptionfolder);

        $this->assertApiResponse($subscriptionfolder);
    }

    /**
     * @test
     */
    public function test_read_subscriptionfolder()
    {
        $subscriptionfolder = $this->makeSubscriptionfolder();
        $this->response = $this->json('GET', '/api/subscriptionfolders/'.$subscriptionfolder->id);

        $this->assertApiResponse($subscriptionfolder->toArray());
    }

    /**
     * @test
     */
    public function test_update_subscriptionfolder()
    {
        $subscriptionfolder = $this->makeSubscriptionfolder();
        $editedSubscriptionfolder = $this->fakeSubscriptionfolderData();

        $this->response = $this->json('PUT', '/api/subscriptionfolders/'.$subscriptionfolder->id, $editedSubscriptionfolder);

        $this->assertApiResponse($editedSubscriptionfolder);
    }

    /**
     * @test
     */
    public function test_delete_subscriptionfolder()
    {
        $subscriptionfolder = $this->makeSubscriptionfolder();
        $this->response = $this->json('DELETE', '/api/subscriptionfolders/'.$subscriptionfolder->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/subscriptionfolders/'.$subscriptionfolder->id);

        $this->response->assertStatus(404);
    }
}
