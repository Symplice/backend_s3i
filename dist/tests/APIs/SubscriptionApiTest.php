<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use $NAMESPACE_TEST_TRAITS$\MakeSubscriptionTrait;
use Tests\ApiTestTrait;

class SubscriptionApiTest extends TestCase
{
    use MakeSubscriptionTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_subscription()
    {
        $subscription = $this->fakeSubscriptionData();
        $this->response = $this->json('POST', '/api/subscriptions', $subscription);

        $this->assertApiResponse($subscription);
    }

    /**
     * @test
     */
    public function test_read_subscription()
    {
        $subscription = $this->makeSubscription();
        $this->response = $this->json('GET', '/api/subscriptions/'.$subscription->id);

        $this->assertApiResponse($subscription->toArray());
    }

    /**
     * @test
     */
    public function test_update_subscription()
    {
        $subscription = $this->makeSubscription();
        $editedSubscription = $this->fakeSubscriptionData();

        $this->response = $this->json('PUT', '/api/subscriptions/'.$subscription->id, $editedSubscription);

        $this->assertApiResponse($editedSubscription);
    }

    /**
     * @test
     */
    public function test_delete_subscription()
    {
        $subscription = $this->makeSubscription();
        $this->response = $this->json('DELETE', '/api/subscriptions/'.$subscription->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/subscriptions/'.$subscription->id);

        $this->response->assertStatus(404);
    }
}
