<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use $NAMESPACE_TEST_TRAITS$\MakeAccommodationtypeTrait;
use Tests\ApiTestTrait;

class AccommodationtypeApiTest extends TestCase
{
    use MakeAccommodationtypeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_accommodationtype()
    {
        $accommodationtype = $this->fakeAccommodationtypeData();
        $this->response = $this->json('POST', '/api/accommodationtypes', $accommodationtype);

        $this->assertApiResponse($accommodationtype);
    }

    /**
     * @test
     */
    public function test_read_accommodationtype()
    {
        $accommodationtype = $this->makeAccommodationtype();
        $this->response = $this->json('GET', '/api/accommodationtypes/'.$accommodationtype->id);

        $this->assertApiResponse($accommodationtype->toArray());
    }

    /**
     * @test
     */
    public function test_update_accommodationtype()
    {
        $accommodationtype = $this->makeAccommodationtype();
        $editedAccommodationtype = $this->fakeAccommodationtypeData();

        $this->response = $this->json('PUT', '/api/accommodationtypes/'.$accommodationtype->id, $editedAccommodationtype);

        $this->assertApiResponse($editedAccommodationtype);
    }

    /**
     * @test
     */
    public function test_delete_accommodationtype()
    {
        $accommodationtype = $this->makeAccommodationtype();
        $this->response = $this->json('DELETE', '/api/accommodationtypes/'.$accommodationtype->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/accommodationtypes/'.$accommodationtype->id);

        $this->response->assertStatus(404);
    }
}
