<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Accommodation;
use Faker\Generator as Faker;

$factory->define(Accommodation::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'slug' => $faker->word,
        'program_id' => $faker->word,
        'accommodation_type_id' => $faker->word,
        'rooms_number' => $faker->randomDigitNotNull,
        'bathroom' => $faker->randomDigitNotNull,
        'lot' => $faker->word,
        'ilot' => $faker->word,
        'parking_place' => $faker->randomDigitNotNull,
        'terrace' => $faker->randomDigitNotNull,
        'dining_room' => $faker->randomDigitNotNull,
        'living_room' => $faker->randomDigitNotNull,
        'price' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'kitchen' => $faker->randomDigitNotNull,
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
