<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Subscription;
use Faker\Generator as Faker;

$factory->define(Subscription::class, function (Faker $faker) {

    return [
        'user_id' => $faker->word,
        'customer_id' => $faker->word,
        'program_id' => $faker->word,
        'accommodation_id' => $faker->word,
        'application_fees' => $faker->randomDigitNotNull,
        'initial_contribution' => $faker->word,
        'delivery_formula' => $faker->word,
        'status' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
