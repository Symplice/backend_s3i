<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Customer;
use Faker\Generator as Faker;

$factory->define(Customer::class, function (Faker $faker) {

    return [
        'user_id' => $faker->word,
        'account_number' => $faker->word,
        'civility' => $faker->word,
        'photo' => $faker->word,
        'name' => $faker->word,
        'firstname' => $faker->word,
        'phone_number' => $faker->word,
        'email' => $faker->word,
        'rib' => $faker->word,
        'document' => $faker->word,
        'document_reference' => $faker->word,
        'nationality' => $faker->word,
        'birthdate' => $faker->word,
        'birth_place' => $faker->word,
        'occupation' => $faker->word,
        'employer' => $faker->word,
        'residency' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
