<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Subscriptionfolder;
use Faker\Generator as Faker;

$factory->define(Subscriptionfolder::class, function (Faker $faker) {

    return [
        'user_id' => $faker->randomDigitNotNull,
        'libelle' => $faker->word,
        'status' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
