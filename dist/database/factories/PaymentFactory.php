<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Payment;
use Faker\Generator as Faker;

$factory->define(Payment::class, function (Faker $faker) {

    return [
        'user_id' => $faker->word,
        'date' => $faker->date('Y-m-d H:i:s'),
        'amount' => $faker->randomDigitNotNull,
        'accommodation_id' => $faker->word,
        'method' => $faker->word,
        'bank_id' => $faker->word,
        'next_payment' => $faker->word,
        'left_to_pay' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
