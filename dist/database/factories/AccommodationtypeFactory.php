<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Accommodationtype;
use Faker\Generator as Faker;

$factory->define(Accommodationtype::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'description' => $faker->word,
        'land_space' => $faker->randomDigitNotNull,
        'built_area' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
