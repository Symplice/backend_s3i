<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {

	Route::post('informations', function (Request $request) {
		$user = $request->user();

	    return response()->json($user,200);
    });
    
    Route::resource('accommodationtypes', 'AccommodationtypeAPIController');

    Route::resource('programs', 'ProgramAPIController');

    Route::resource('accommodations', 'AccommodationAPIController');

    Route::resource('albumphotos', 'AlbumphotoAPIController');

    Route::resource('categories', 'CategoryAPIController');

    Route::resource('articles', 'ArticleAPIController');

    Route::resource('banks', 'BankAPIController');

    Route::resource('customers', 'CustomerAPIController');

    Route::resource('images', 'ImageAPIController');

    Route::resource('messages', 'MessageAPIController');

    Route::resource('payments', 'PaymentAPIController');

    Route::resource('subscriptions', 'SubscriptionAPIController');

    Route::resource('subscriptionfolders', 'SubscriptionfolderAPIController');
});