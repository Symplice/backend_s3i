<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});


Auth::routes(['register'=>false]);

Route::post('api/login', 'Auth\LoginController@loginApi');
Route::post('api/refresh', 'Auth\LoginController@refresh');

Route::middleware('auth:api')->group(function () {
	Route::post('api/logout', 'Auth\LoginController@logoutApi');
});

Route::get('/home', 'HomeController@index');

Route::group(['middleware'=> 'auth'],function(){

	Route::group(['middleware' => ['role:supadmin']], function () {

		//Roles
		Route::resource('roles', 'RoleController');
		Route::post('roles/addPermission', 'RoleController@addPermission');
		Route::get('roles/removePermission/{permission}/{role_id}', 'RoleController@revokePermission');

		//Permissions
		Route::resource('permissions', 'PermissionController');

		Route::resource('users', 'UserController');
		Route::post('users/addRole', 'UserController@addRole');
		Route::post('users/addPermission', 'UserController@addPermission');
		Route::get('users/removePermission/{permission}/{user_id}', 'UserController@revokePermission');
		Route::get('users/removeRole/{role}/{user_id}', 'UserController@revokeRole');
    });

    Route::resource('accommodationtypes', 'AccommodationtypeController');

    Route::resource('programs', 'ProgramController');

    Route::resource('accommodations', 'AccommodationController');

    Route::resource('albumphotos', 'AlbumphotoController');

    Route::resource('categories', 'CategoryController');

    Route::resource('articles', 'ArticleController');

    Route::resource('banks', 'BankController');

    Route::resource('customers', 'CustomerController');

    Route::resource('images', 'ImageController');

    Route::resource('messages', 'MessageController');

    Route::resource('payments', 'PaymentController');

    Route::resource('subscriptions', 'SubscriptionController');

    Route::resource('subscriptionfolders', 'SubscriptionfolderController');
    
});

