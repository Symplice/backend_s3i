<?php

namespace App\Repositories;

use App\Models\Albumphoto;
use App\Repositories\BaseRepository;

/**
 * Class AlbumphotoRepository
 * @package App\Repositories
 * @version February 6, 2021, 3:45 pm UTC
*/

class AlbumphotoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'accommodation_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Albumphoto::class;
    }
}
