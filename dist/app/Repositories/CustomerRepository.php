<?php

namespace App\Repositories;

use App\Models\Customer;
use App\Repositories\BaseRepository;

/**
 * Class CustomerRepository
 * @package App\Repositories
 * @version February 6, 2021, 3:48 pm UTC
*/

class CustomerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'account_number',
        'civility',
        'photo',
        'name',
        'firstname',
        'phone_number',
        'email',
        'rib',
        'document',
        'document_reference',
        'nationality',
        'birthdate',
        'birth_place',
        'occupation',
        'employer',
        'residency'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Customer::class;
    }
}
