<?php

namespace App\Repositories;

use App\Models\Accommodationtype;
use App\Repositories\BaseRepository;

/**
 * Class AccommodationtypeRepository
 * @package App\Repositories
 * @version February 6, 2021, 3:34 pm UTC
*/

class AccommodationtypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'land_space',
        'built_area'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Accommodationtype::class;
    }
}
