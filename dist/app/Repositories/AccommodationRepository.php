<?php

namespace App\Repositories;

use App\Models\Accommodation;
use App\Repositories\BaseRepository;

/**
 * Class AccommodationRepository
 * @package App\Repositories
 * @version February 6, 2021, 3:45 pm UTC
*/

class AccommodationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug',
        'program_id',
        'accommodation_type_id',
        'rooms_number',
        'bathroom',
        'lot',
        'ilot',
        'parking_place',
        'terrace',
        'dining_room',
        'living_room',
        'price',
        'kitchen'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Accommodation::class;
    }
}
