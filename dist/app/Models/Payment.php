<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Payment
 * @package App\Models
 * @version February 6, 2021, 3:51 pm UTC
 *
 * @property \App\Models\Accommodation $accommodation
 * @property \App\Models\User $user
 * @property \App\Models\Bank $bank
 * @property integer $user_id
 * @property string|\Carbon\Carbon $date
 * @property number $amount
 * @property integer $accommodation_id
 * @property string $method
 * @property integer $bank_id
 * @property string $next_payment
 * @property number $left_to_pay
 */
class Payment extends Model
{
    use SoftDeletes;

    public $table = 'payments';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'date',
        'amount',
        'accommodation_id',
        'method',
        'bank_id',
        'next_payment',
        'left_to_pay'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'date' => 'datetime',
        'amount' => 'float',
        'accommodation_id' => 'integer',
        'method' => 'string',
        'bank_id' => 'integer',
        'next_payment' => 'date',
        'left_to_pay' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'date' => 'required',
        'amount' => 'required',
        'accommodation_id' => 'required',
        'method' => 'required',
        'bank_id' => 'required',
        'left_to_pay' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function subscription()
    {
        return $this->belongsTo(\App\Models\Subscription::class, 'subscription_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function bank()
    {
        return $this->belongsTo(\App\Models\Bank::class, 'bank_id');
    }
}
