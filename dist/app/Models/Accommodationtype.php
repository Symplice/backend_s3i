<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Accommodationtype
 * @package App\Models
 * @version February 6, 2021, 3:34 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $accommodations
 * @property string $name
 * @property string $description
 * @property number $land_space
 * @property number $built_area
 */
class Accommodationtype extends Model
{
    use SoftDeletes;

    public $table = 'accommodation_type';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'description',
        'land_space',
        'built_area'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'land_space' => 'float',
        'built_area' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'land_space' => 'required',
        'built_area' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function accommodations()
    {
        return $this->hasMany(\App\Models\Accommodation::class, 'accommodation_type_id');
    }
}
