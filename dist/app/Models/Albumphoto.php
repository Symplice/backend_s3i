<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Albumphoto
 * @package App\Models
 * @version February 6, 2021, 3:45 pm UTC
 *
 * @property \App\Models\Accommodation $accommodation
 * @property \Illuminate\Database\Eloquent\Collection $images
 * @property string $name
 * @property integer $accommodation_id
 */
class Albumphoto extends Model
{
    use SoftDeletes;

    public $table = 'album_photos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'accommodation_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'accommodation_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'accommodation_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function accommodation()
    {
        return $this->belongsTo(\App\Models\Accommodation::class, 'accommodation_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function images()
    {
        return $this->hasMany(\App\Models\Image::class, 'album_photo_id');
    }
}
