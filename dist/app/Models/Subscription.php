<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Subscription
 * @package App\Models
 * @version February 6, 2021, 3:52 pm UTC
 *
 * @property \App\Models\User $user
 * @property \App\Models\Program $program
 * @property \App\Models\Accommodation $accommodation
 * @property \App\Models\Customer $customer
 * @property integer $user_id
 * @property integer $customer_id
 * @property integer $program_id
 * @property integer $accommodation_id
 * @property number $application_fees
 * @property string $initial_contribution
 * @property string $delivery_formula
 * @property string $status
 */
class Subscription extends Model
{
    use SoftDeletes;

    public $table = 'subscriptions';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'customer_id',
        'program_id',
        'accommodation_id',
        'application_fees',
        'initial_contribution',
        'delivery_formula',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'customer_id' => 'integer',
        'program_id' => 'integer',
        'accommodation_id' => 'integer',
        'application_fees' => 'float',
        'initial_contribution' => 'string',
        'delivery_formula' => 'string',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'customer_id' => 'required',
        'program_id' => 'required',
        'accommodation_id' => 'required',
        'application_fees' => 'required',
        'initial_contribution' => 'required',
        'delivery_formula' => 'required',
        'status' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function program()
    {
        return $this->belongsTo(\App\Models\Program::class, 'program_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function payments()
    {
        return $this->hasMany(\App\Models\Payment::class, 'accommodation_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }
}
