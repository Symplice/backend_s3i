<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Program
 * @package App\Models
 * @version February 6, 2021, 3:44 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $accommodations
 * @property \Illuminate\Database\Eloquent\Collection $subscriptions
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property string $city
 * @property string $town
 * @property integer $accommodation_number
 */
class Program extends Model
{
    use SoftDeletes;

    public $table = 'programs';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'slug',
        'description',
        'city',
        'town',
        'accommodation_number'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'description' => 'string',
        'city' => 'string',
        'town' => 'string',
        'accommodation_number' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'slug' => 'required',
        'description' => 'required',
        'city' => 'required',
        'accommodation_number' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function accommodations()
    {
        return $this->hasMany(\App\Models\Accommodation::class, 'program_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function subscriptions()
    {
        return $this->hasMany(\App\Models\Subscription::class, 'program_id');
    }
}
