<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Customer
 * @package App\Models
 * @version February 6, 2021, 3:48 pm UTC
 *
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $subscriptions
 * @property integer $user_id
 * @property string $account_number
 * @property string $civility
 * @property string $photo
 * @property string $name
 * @property string $firstname
 * @property string $phone_number
 * @property string $email
 * @property string $rib
 * @property string $document
 * @property string $document_reference
 * @property string $nationality
 * @property string $birthdate
 * @property string $birth_place
 * @property string $occupation
 * @property string $employer
 * @property string $residency
 */
class Customer extends Model
{
    use SoftDeletes;

    public $table = 'customers';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'account_number',
        'civility',
        'photo',
        'name',
        'firstname',
        'phone_number',
        'email',
        'rib',
        'document',
        'document_reference',
        'nationality',
        'birthdate',
        'birth_place',
        'occupation',
        'employer',
        'residency'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'account_number' => 'string',
        'civility' => 'string',
        'photo' => 'string',
        'name' => 'string',
        'firstname' => 'string',
        'phone_number' => 'string',
        'email' => 'string',
        'rib' => 'string',
        'document' => 'string',
        'document_reference' => 'string',
        'nationality' => 'string',
        'birthdate' => 'date',
        'birth_place' => 'string',
        'occupation' => 'string',
        'employer' => 'string',
        'residency' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'account_number' => 'required',
        'civility' => 'required',
        'name' => 'required',
        'firstname' => 'required',
        'phone_number' => 'required',
        'email' => 'required',
        'rib' => 'required',
        'document' => 'required',
        'document_reference' => 'required',
        'nationality' => 'required',
        'birthdate' => 'required',
        'birth_place' => 'required',
        'occupation' => 'required',
        'residency' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function subscriptions()
    {
        return $this->hasMany(\App\Models\Subscription::class, 'customer_id');
    }
}
