<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Accommodation
 * @package App\Models
 * @version February 6, 2021, 3:45 pm UTC
 *
 * @property \App\Models\Program $program
 * @property \App\Models\AccommodationType $accommodationType
 * @property \Illuminate\Database\Eloquent\Collection $albumPhotos
 * @property \Illuminate\Database\Eloquent\Collection $payments
 * @property \Illuminate\Database\Eloquent\Collection $subscriptions
 * @property string $name
 * @property string $slug
 * @property integer $program_id
 * @property integer $accommodation_type_id
 * @property integer $rooms_number
 * @property integer $bathroom
 * @property string $lot
 * @property string $ilot
 * @property integer $parking_place
 * @property integer $terrace
 * @property integer $dining_room
 * @property integer $living_room
 * @property number $price
 * @property number $kitchen
 */
class Accommodation extends Model
{
    use SoftDeletes;

    public $table = 'accommodations';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'slug',
        'program_id',
        'accommodation_type_id',
        'rooms_number',
        'bathroom',
        'lot',
        'ilot',
        'parking_place',
        'terrace',
        'dining_room',
        'living_room',
        'price',
        'kitchen'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'program_id' => 'integer',
        'accommodation_type_id' => 'integer',
        'rooms_number' => 'integer',
        'bathroom' => 'integer',
        'lot' => 'string',
        'ilot' => 'string',
        'parking_place' => 'integer',
        'terrace' => 'integer',
        'dining_room' => 'integer',
        'living_room' => 'integer',
        'price' => 'float',
        'kitchen' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'slug' => 'required',
        'program_id' => 'required',
        'accommodation_type_id' => 'required',
        'rooms_number' => 'required',
        'bathroom' => 'required',
        'lot' => 'required',
        'ilot' => 'required',
        'parking_place' => 'required',
        'terrace' => 'required',
        'dining_room' => 'required',
        'living_room' => 'required',
        'price' => 'required',
        'kitchen' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function program()
    {
        return $this->belongsTo(\App\Models\Program::class, 'program_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function accommodationType()
    {
        return $this->belongsTo(\App\Models\AccommodationType::class, 'accommodation_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function albumPhotos()
    {
        return $this->hasMany(\App\Models\AlbumPhoto::class, 'accommodation_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function subscriptions()
    {
        return $this->hasMany(\App\Models\Subscription::class, 'accommodation_id');
    }
}
