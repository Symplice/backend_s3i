<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSubscriptionfolderRequest;
use App\Http\Requests\UpdateSubscriptionfolderRequest;
use App\Repositories\SubscriptionfolderRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class SubscriptionfolderController extends AppBaseController
{
    /** @var  SubscriptionfolderRepository */
    private $subscriptionfolderRepository;

    public function __construct(SubscriptionfolderRepository $subscriptionfolderRepo)
    {
        $this->subscriptionfolderRepository = $subscriptionfolderRepo;
    }

    /**
     * Display a listing of the Subscriptionfolder.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $subscriptionfolders = $this->subscriptionfolderRepository->all();

        return view('subscriptionfolders.index',compact('subscriptionfolders'));
    }

    /**
     * Show the form for creating a new Subscriptionfolder.
     *
     * @return Response
     */
    public function create()
    {
        return view('subscriptionfolders.create');
    }

    /**
     * Store a newly created Subscriptionfolder in storage.
     *
     * @param CreateSubscriptionfolderRequest $request
     *
     * @return Response
     */
    public function store(CreateSubscriptionfolderRequest $request)
    {
        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $subscriptionfolder = $this->subscriptionfolderRepository->create($input);

            return $subscriptionfolder;
        });
        
        if($success){
            Flash::success('Subscriptionfolder a bien été ajouté.');

            return redirect()->action('SubscriptionfolderController@create');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('SubscriptionfolderController@create')->withInput();
        }
    }

    /**
     * Display the specified Subscriptionfolder.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $subscriptionfolder = $this->subscriptionfolderRepository->find($id);

        if (empty($subscriptionfolder)) {
            Flash::error('Subscriptionfolder not found');

            return redirect()->action('SubscriptionfolderController@index');
        }

        return view('subscriptionfolders.show',compact('subscriptionfolder'));
    }

    /**
     * Show the form for editing the specified Subscriptionfolder.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $subscriptionfolder = $this->subscriptionfolderRepository->find($id);

        if (empty($subscriptionfolder)) {
            Flash::error('Subscriptionfolder not found');

            return redirect()->action('SubscriptionfolderController@index');
        }

        return view('subscriptionfolders.edit',compact('subscriptionfolder'));
    }

    /**
     * Update the specified Subscriptionfolder in storage.
     *
     * @param  int              $id
     * @param UpdateSubscriptionfolderRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSubscriptionfolderRequest $request)
    {
        $subscriptionfolder = $this->subscriptionfolderRepository->find($id);

        if (empty($subscriptionfolder)) {
            Flash::error('Subscriptionfolder not found');

            return redirect()->action('SubscriptionfolderController@create');
        }

        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input,$id){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $subscriptionfolder = $this->subscriptionfolderRepository->update($input, $id);

            return $subscriptionfolder;
        });
        
        if($success){
            Flash::success('Subscriptionfolder a bien été modifié.');

            return redirect()->action('SubscriptionfolderController@edit',['subscriptionfolder'=>$id]);
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('SubscriptionfolderController@edit',['subscriptionfolder'=>$id])->withInput();
        }
    }

    /**
     * Remove the specified Subscriptionfolder from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $subscriptionfolder = $this->subscriptionfolderRepository->find($id);

        if (empty($subscriptionfolder)) {
            Flash::error('Subscriptionfolder not found');

            return redirect()->action('SubscriptionfolderController@index');
        }

        $success = false;
            
        $success = \DB::transaction(function() use ($id){

            $this->subscriptionfolderRepository->delete($id);

            return true;
        });
        
        if($success){
            Flash::success('Subscriptionfolder a bien été supprimé.');

            return redirect()->action('SubscriptionfolderController@index');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('SubscriptionfolderController@index');
        }
    }
}
