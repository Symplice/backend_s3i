<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBankRequest;
use App\Http\Requests\UpdateBankRequest;
use App\Repositories\BankRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class BankController extends AppBaseController
{
    /** @var  BankRepository */
    private $bankRepository;

    public function __construct(BankRepository $bankRepo)
    {
        $this->bankRepository = $bankRepo;
    }

    /**
     * Display a listing of the Bank.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $banks = $this->bankRepository->all();

        return view('banks.index',compact('banks'));
    }

    /**
     * Show the form for creating a new Bank.
     *
     * @return Response
     */
    public function create()
    {
        return view('banks.create');
    }

    /**
     * Store a newly created Bank in storage.
     *
     * @param CreateBankRequest $request
     *
     * @return Response
     */
    public function store(CreateBankRequest $request)
    {
        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $bank = $this->bankRepository->create($input);

            return $bank;
        });
        
        if($success){
            Flash::success('Bank a bien été ajouté.');

            return redirect()->action('BankController@create');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('BankController@create')->withInput();
        }
    }

    /**
     * Display the specified Bank.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $bank = $this->bankRepository->find($id);

        if (empty($bank)) {
            Flash::error('Bank not found');

            return redirect()->action('BankController@index');
        }

        return view('banks.show',compact('bank'));
    }

    /**
     * Show the form for editing the specified Bank.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $bank = $this->bankRepository->find($id);

        if (empty($bank)) {
            Flash::error('Bank not found');

            return redirect()->action('BankController@index');
        }

        return view('banks.edit',compact('bank'));
    }

    /**
     * Update the specified Bank in storage.
     *
     * @param  int              $id
     * @param UpdateBankRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBankRequest $request)
    {
        $bank = $this->bankRepository->find($id);

        if (empty($bank)) {
            Flash::error('Bank not found');

            return redirect()->action('BankController@create');
        }

        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input,$id){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $bank = $this->bankRepository->update($input, $id);

            return $bank;
        });
        
        if($success){
            Flash::success('Bank a bien été modifié.');

            return redirect()->action('BankController@edit',['bank'=>$id]);
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('BankController@edit',['bank'=>$id])->withInput();
        }
    }

    /**
     * Remove the specified Bank from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $bank = $this->bankRepository->find($id);

        if (empty($bank)) {
            Flash::error('Bank not found');

            return redirect()->action('BankController@index');
        }

        $success = false;
            
        $success = \DB::transaction(function() use ($id){

            $this->bankRepository->delete($id);

            return true;
        });
        
        if($success){
            Flash::success('Bank a bien été supprimé.');

            return redirect()->action('BankController@index');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('BankController@index');
        }
    }
}
