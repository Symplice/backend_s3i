<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Repositories\CategoryRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class CategoryController extends AppBaseController
{
    /** @var  CategoryRepository */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepo)
    {
        $this->categoryRepository = $categoryRepo;
    }

    /**
     * Display a listing of the Category.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $categories = $this->categoryRepository->all();

        return view('categories.index',compact('categories'));
    }

    /**
     * Show the form for creating a new Category.
     *
     * @return Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created Category in storage.
     *
     * @param CreateCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoryRequest $request)
    {
        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $category = $this->categoryRepository->create($input);

            return $category;
        });
        
        if($success){
            Flash::success('Category a bien été ajouté.');

            return redirect()->action('CategoryController@create');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('CategoryController@create')->withInput();
        }
    }

    /**
     * Display the specified Category.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $category = $this->categoryRepository->find($id);

        if (empty($category)) {
            Flash::error('Category not found');

            return redirect()->action('CategoryController@index');
        }

        return view('categories.show',compact('category'));
    }

    /**
     * Show the form for editing the specified Category.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $category = $this->categoryRepository->find($id);

        if (empty($category)) {
            Flash::error('Category not found');

            return redirect()->action('CategoryController@index');
        }

        return view('categories.edit',compact('category'));
    }

    /**
     * Update the specified Category in storage.
     *
     * @param  int              $id
     * @param UpdateCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryRequest $request)
    {
        $category = $this->categoryRepository->find($id);

        if (empty($category)) {
            Flash::error('Category not found');

            return redirect()->action('CategoryController@create');
        }

        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input,$id){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $category = $this->categoryRepository->update($input, $id);

            return $category;
        });
        
        if($success){
            Flash::success('Category a bien été modifié.');

            return redirect()->action('CategoryController@edit',['category'=>$id]);
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('CategoryController@edit',['category'=>$id])->withInput();
        }
    }

    /**
     * Remove the specified Category from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $category = $this->categoryRepository->find($id);

        if (empty($category)) {
            Flash::error('Category not found');

            return redirect()->action('CategoryController@index');
        }

        $success = false;
            
        $success = \DB::transaction(function() use ($id){

            $this->categoryRepository->delete($id);

            return true;
        });
        
        if($success){
            Flash::success('Category a bien été supprimé.');

            return redirect()->action('CategoryController@index');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('CategoryController@index');
        }
    }
}
