<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use App\Models\User;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Hash;
use Validator;

class UserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->userRepository->pushCriteria(new RequestCriteria($request));
        $users = $this->userRepository->all();

        return view('users.index',compact('users'));
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        $roles = Role::get();
        return view('users.create',compact('roles'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $input['password'] = Hash::make($input['password']);

            $user = $this->userRepository->create($input);
            $user->assignRole($request->input('roles'));
            return $user;
        });
        
        if($success){
            Flash::success('L\'utilisateur a bien été ajouté.');

            return redirect(route('users.index'));
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect(route('users.create'));
        }
    }

    /**
     * Display the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        return view('users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        if (!$user) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }
        $roles = Role::whereNotIn('name',['supadmin'])->get()->pluck('label','name');
        $userRoles = $user->roles;

        return view('users.edit',compact('user','roles','userRoles'));
    }

    /**
     * Update the specified User in storage.
     *
     * @param  int              $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        $input = $request->all();
        $input=array_map('trim',$input);

        User::$rules['password'] = 'min:8';

        if ($request->has('email')) {
            if ($user->email == $input['email']) {
                User::$rules['email'] = 'required|email|max:255|exists:users';
            }   
        }

        $validation = Validator::make($input, User::$rules);

        if ($validation->passes())
        {

            $input = $request->all();

            $success = false;
                
            $success = \DB::transaction(function() use ($request,$input,$id){

                foreach ($input as $k => $v) {
                    if (!$request->filled($k)) {
                        unset($input[$k]);
                    }
                }

                if ($request->filled('password')) {
                    
                    $input['password'] = Hash::make($input['password']);
                }

                $user = $this->userRepository->update($input, $id);

                return $user;
            });
            
            if($success){
                Flash::success('L\'utilisateur a bien été modifié.');

                return redirect(route('users.edit',['user'=>$id]));
            }else{
                Flash::error('Une erreur est survenue.');

                return redirect(route('users.edit',['user'=>$id]));
            }
        }

        Flash::error('Erreur de validation.');

        return redirect(route('users.edit',['user'=>$id]))->withInput()->withErrors($validation);
    }

    /**
     * Remove the specified User from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->findWithoutFail($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        $success = false;
            
        $success = \DB::transaction(function() use ($id){

            $this->userRepository->delete($id);

            return $user;
        });
        
        if($success){
            Flash::success('L\'utilisateur a bien été supprimé.');

            return redirect(route('users.index'));
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect(route('users.index'));
        }
    }

    /**
     * Assign Role to user.
     *
     * @param \Illuminate\Http\Request
     *
     * @return \Illuminate\Http\Response
     */
    public function addRole(Request $request)
    {
        $user = \App\User::findOrfail($request->user_id);
        $user->assignRole($request->role_name);

        return redirect()->action('UserController@edit',['user'=>$request->user_id]);
    }

    /**
     * Assign Permission to a user.
     *
     * @param \Illuminate\Http\Request
     *
     * @return \Illuminate\Http\Response
     */
    public function addPermission(Request $request)
    {
        $user = \App\User::findorfail($request->user_id);
        $user->givePermissionTo($request->permission_name);

        return redirect()->action('UserController@edit',['user'=>$request->user_id]);
    }

    /**
     * revoke Permission to a user.
     *
     * @param \Illuminate\Http\Request
     *
     * @return \Illuminate\Http\Response
     */
    public function revokePermission($permission, $user_id)
    {
        $user = \App\User::findorfail($user_id);

        $user->revokePermissionTo(str_slug($permission, '_'));

        return redirect()->action('UserController@edit',['user'=>$user_id]);
    }

    /**
     * revoke Role to a a user.
     *
     * @param \Illuminate\Http\Request
     *
     * @return \Illuminate\Http\Response
     */
    public function revokeRole($role, $user_id)
    {
        $user = \App\User::findorfail($user_id);

        $user->removeRole(str_slug($role, '_'));

        return redirect()->action('UserController@edit',['user'=>$user_id]);
    }
}
