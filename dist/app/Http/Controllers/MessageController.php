<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateMessageRequest;
use App\Http\Requests\UpdateMessageRequest;
use App\Repositories\MessageRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class MessageController extends AppBaseController
{
    /** @var  MessageRepository */
    private $messageRepository;

    public function __construct(MessageRepository $messageRepo)
    {
        $this->messageRepository = $messageRepo;
    }

    /**
     * Display a listing of the Message.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $messages = $this->messageRepository->all();

        return view('messages.index',compact('messages'));
    }

    /**
     * Show the form for creating a new Message.
     *
     * @return Response
     */
    public function create()
    {
        return view('messages.create');
    }

    /**
     * Store a newly created Message in storage.
     *
     * @param CreateMessageRequest $request
     *
     * @return Response
     */
    public function store(CreateMessageRequest $request)
    {
        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $message = $this->messageRepository->create($input);

            return $message;
        });
        
        if($success){
            Flash::success('Message a bien été ajouté.');

            return redirect()->action('MessageController@create');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('MessageController@create')->withInput();
        }
    }

    /**
     * Display the specified Message.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $message = $this->messageRepository->find($id);

        if (empty($message)) {
            Flash::error('Message not found');

            return redirect()->action('MessageController@index');
        }

        return view('messages.show',compact('message'));
    }

    /**
     * Show the form for editing the specified Message.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $message = $this->messageRepository->find($id);

        if (empty($message)) {
            Flash::error('Message not found');

            return redirect()->action('MessageController@index');
        }

        return view('messages.edit',compact('message'));
    }

    /**
     * Update the specified Message in storage.
     *
     * @param  int              $id
     * @param UpdateMessageRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMessageRequest $request)
    {
        $message = $this->messageRepository->find($id);

        if (empty($message)) {
            Flash::error('Message not found');

            return redirect()->action('MessageController@create');
        }

        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input,$id){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $message = $this->messageRepository->update($input, $id);

            return $message;
        });
        
        if($success){
            Flash::success('Message a bien été modifié.');

            return redirect()->action('MessageController@edit',['message'=>$id]);
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('MessageController@edit',['message'=>$id])->withInput();
        }
    }

    /**
     * Remove the specified Message from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $message = $this->messageRepository->find($id);

        if (empty($message)) {
            Flash::error('Message not found');

            return redirect()->action('MessageController@index');
        }

        $success = false;
            
        $success = \DB::transaction(function() use ($id){

            $this->messageRepository->delete($id);

            return true;
        });
        
        if($success){
            Flash::success('Message a bien été supprimé.');

            return redirect()->action('MessageController@index');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('MessageController@index');
        }
    }
}
