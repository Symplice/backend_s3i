<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAlbumphotoRequest;
use App\Http\Requests\UpdateAlbumphotoRequest;
use App\Repositories\AlbumphotoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class AlbumphotoController extends AppBaseController
{
    /** @var  AlbumphotoRepository */
    private $albumphotoRepository;

    public function __construct(AlbumphotoRepository $albumphotoRepo)
    {
        $this->albumphotoRepository = $albumphotoRepo;
    }

    /**
     * Display a listing of the Albumphoto.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $albumphotos = $this->albumphotoRepository->all();

        return view('albumphotos.index',compact('albumphotos'));
    }

    /**
     * Show the form for creating a new Albumphoto.
     *
     * @return Response
     */
    public function create()
    {
        return view('albumphotos.create');
    }

    /**
     * Store a newly created Albumphoto in storage.
     *
     * @param CreateAlbumphotoRequest $request
     *
     * @return Response
     */
    public function store(CreateAlbumphotoRequest $request)
    {
        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $albumphoto = $this->albumphotoRepository->create($input);

            return $albumphoto;
        });
        
        if($success){
            Flash::success('Albumphoto a bien été ajouté.');

            return redirect()->action('AlbumphotoController@create');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('AlbumphotoController@create')->withInput();
        }
    }

    /**
     * Display the specified Albumphoto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $albumphoto = $this->albumphotoRepository->find($id);

        if (empty($albumphoto)) {
            Flash::error('Albumphoto not found');

            return redirect()->action('AlbumphotoController@index');
        }

        return view('albumphotos.show',compact('albumphoto'));
    }

    /**
     * Show the form for editing the specified Albumphoto.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $albumphoto = $this->albumphotoRepository->find($id);

        if (empty($albumphoto)) {
            Flash::error('Albumphoto not found');

            return redirect()->action('AlbumphotoController@index');
        }

        return view('albumphotos.edit',compact('albumphoto'));
    }

    /**
     * Update the specified Albumphoto in storage.
     *
     * @param  int              $id
     * @param UpdateAlbumphotoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAlbumphotoRequest $request)
    {
        $albumphoto = $this->albumphotoRepository->find($id);

        if (empty($albumphoto)) {
            Flash::error('Albumphoto not found');

            return redirect()->action('AlbumphotoController@create');
        }

        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input,$id){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $albumphoto = $this->albumphotoRepository->update($input, $id);

            return $albumphoto;
        });
        
        if($success){
            Flash::success('Albumphoto a bien été modifié.');

            return redirect()->action('AlbumphotoController@edit',['albumphoto'=>$id]);
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('AlbumphotoController@edit',['albumphoto'=>$id])->withInput();
        }
    }

    /**
     * Remove the specified Albumphoto from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $albumphoto = $this->albumphotoRepository->find($id);

        if (empty($albumphoto)) {
            Flash::error('Albumphoto not found');

            return redirect()->action('AlbumphotoController@index');
        }

        $success = false;
            
        $success = \DB::transaction(function() use ($id){

            $this->albumphotoRepository->delete($id);

            return true;
        });
        
        if($success){
            Flash::success('Albumphoto a bien été supprimé.');

            return redirect()->action('AlbumphotoController@index');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('AlbumphotoController@index');
        }
    }
}
