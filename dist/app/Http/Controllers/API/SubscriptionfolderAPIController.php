<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSubscriptionfolderAPIRequest;
use App\Http\Requests\API\UpdateSubscriptionfolderAPIRequest;
use App\Models\Subscriptionfolder;
use App\Repositories\SubscriptionfolderRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Validator;

/**
 * Class SubscriptionfolderController
 * @package App\Http\Controllers\API
 */

class SubscriptionfolderAPIController extends AppBaseController
{
    /** @var  SubscriptionfolderRepository */
    private $subscriptionfolderRepository;

    public function __construct(SubscriptionfolderRepository $subscriptionfolderRepo)
    {
        $this->subscriptionfolderRepository = $subscriptionfolderRepo;
    }

    /**
     * Display a listing of the Subscriptionfolder.
     * GET|HEAD /subscriptionfolders
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $subscriptionfolders = $this->subscriptionfolderRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($subscriptionfolders->toArray(), 'Subscriptionfolders retrieved successfully');
    }

    /**
     * Store a newly created Subscriptionfolder in storage.
     * POST /subscriptionfolders
     *
     * @param CreateSubscriptionfolderAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSubscriptionfolderAPIRequest $request)
    {
        $input = $request->all();
        $input=array_map('trim',$input);

        $success = false;
        
        $success = \DB::transaction(function() use ($request,$input){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $subscriptionfolders = $this->subscriptionfolderRepository->create($input);

            return $subscriptionfolders;
        });
        
        if($success){
            return $this->sendResponse($success->toArray(), 'Subscriptionfolder saved successfully');
        }else{
            return $this->sendError(array(), 'Une erreur est survenue.');
        }
    }

    /**
     * Display the specified Subscriptionfolder.
     * GET|HEAD /subscriptionfolders/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Subscriptionfolder $subscriptionfolder */
        $subscriptionfolder = $this->subscriptionfolderRepository->find($id);

        if (empty($subscriptionfolder)) {
            return $this->sendError(array(),'Subscriptionfolder not found');
        }

        return $this->sendResponse($subscriptionfolder->toArray(), 'Subscriptionfolder retrieved successfully');
    }

    /**
     * Update the specified Subscriptionfolder in storage.
     * PUT/PATCH /subscriptionfolders/{id}
     *
     * @param int $id
     * @param UpdateSubscriptionfolderAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSubscriptionfolderAPIRequest $request)
    {

        /** @var Subscriptionfolder $subscriptionfolder */
        $subscriptionfolder = $this->subscriptionfolderRepository->find($id);

        if (empty($subscriptionfolder)) {
            return $this->sendError(array(),'Subscriptionfolder not found');
        }

        $input = $request->all();
        $input=array_map('trim',$input);

        $success = false;
        
        $success = \DB::transaction(function() use ($id,$input,$request){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $subscriptionfolder = $this->subscriptionfolderRepository->update($input, $id);

            return $subscriptionfolder; 
        });
        
        if($success){
            return $this->sendResponse($success->toArray(), 'Subscriptionfolder updated successfully');
        }else{
            return $this->sendError(array(), 'Une erreur est survenue.');
        }
    }

    /**
     * Remove the specified Subscriptionfolder from storage.
     * DELETE /subscriptionfolders/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Subscriptionfolder $subscriptionfolder */
        $subscriptionfolder = $this->subscriptionfolderRepository->find($id);

        if (empty($subscriptionfolder)) {
            return $this->sendError(array(),'Subscriptionfolder not found');
        }

        $success = false;
        
        $success = \DB::transaction(function() use ($subscriptionfolder){
            $subscriptionfolder->delete();

            return true; 
        });
        
        if($success){
            return $this->sendResponse(array(), 'Subscriptionfolder deleted successfully');
        }else{
            return $this->sendError(array(), 'Une erreur est survenue.');
        }

    }
}
