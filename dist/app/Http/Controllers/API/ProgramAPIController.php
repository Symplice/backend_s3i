<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateProgramAPIRequest;
use App\Http\Requests\API\UpdateProgramAPIRequest;
use App\Models\Program;
use App\Repositories\ProgramRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Validator;

/**
 * Class ProgramController
 * @package App\Http\Controllers\API
 */

class ProgramAPIController extends AppBaseController
{
    /** @var  ProgramRepository */
    private $programRepository;

    public function __construct(ProgramRepository $programRepo)
    {
        $this->programRepository = $programRepo;
    }

    /**
     * Display a listing of the Program.
     * GET|HEAD /programs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $programs = $this->programRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($programs->toArray(), 'Programs retrieved successfully');
    }

    /**
     * Store a newly created Program in storage.
     * POST /programs
     *
     * @param CreateProgramAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateProgramAPIRequest $request)
    {
        $input = $request->all();
        $input=array_map('trim',$input);

        $success = false;
        
        $success = \DB::transaction(function() use ($request,$input){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $programs = $this->programRepository->create($input);

            return $programs;
        });
        
        if($success){
            return $this->sendResponse($success->toArray(), 'Program saved successfully');
        }else{
            return $this->sendError(array(), 'Une erreur est survenue.');
        }
    }

    /**
     * Display the specified Program.
     * GET|HEAD /programs/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Program $program */
        $program = $this->programRepository->find($id);

        if (empty($program)) {
            return $this->sendError(array(),'Program not found');
        }

        return $this->sendResponse($program->toArray(), 'Program retrieved successfully');
    }

    /**
     * Update the specified Program in storage.
     * PUT/PATCH /programs/{id}
     *
     * @param int $id
     * @param UpdateProgramAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProgramAPIRequest $request)
    {

        /** @var Program $program */
        $program = $this->programRepository->find($id);

        if (empty($program)) {
            return $this->sendError(array(),'Program not found');
        }

        $input = $request->all();
        $input=array_map('trim',$input);

        $success = false;
        
        $success = \DB::transaction(function() use ($id,$input,$request){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $program = $this->programRepository->update($input, $id);

            return $program; 
        });
        
        if($success){
            return $this->sendResponse($success->toArray(), 'Program updated successfully');
        }else{
            return $this->sendError(array(), 'Une erreur est survenue.');
        }
    }

    /**
     * Remove the specified Program from storage.
     * DELETE /programs/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Program $program */
        $program = $this->programRepository->find($id);

        if (empty($program)) {
            return $this->sendError(array(),'Program not found');
        }

        $success = false;
        
        $success = \DB::transaction(function() use ($program){
            $program->delete();

            return true; 
        });
        
        if($success){
            return $this->sendResponse(array(), 'Program deleted successfully');
        }else{
            return $this->sendError(array(), 'Une erreur est survenue.');
        }

    }
}
