<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateImageAPIRequest;
use App\Http\Requests\API\UpdateImageAPIRequest;
use App\Models\Image;
use App\Repositories\ImageRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Validator;

/**
 * Class ImageController
 * @package App\Http\Controllers\API
 */

class ImageAPIController extends AppBaseController
{
    /** @var  ImageRepository */
    private $imageRepository;

    public function __construct(ImageRepository $imageRepo)
    {
        $this->imageRepository = $imageRepo;
    }

    /**
     * Display a listing of the Image.
     * GET|HEAD /images
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $images = $this->imageRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($images->toArray(), 'Images retrieved successfully');
    }

    /**
     * Store a newly created Image in storage.
     * POST /images
     *
     * @param CreateImageAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateImageAPIRequest $request)
    {
        $input = $request->all();
        $input=array_map('trim',$input);

        $success = false;
        
        $success = \DB::transaction(function() use ($request,$input){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $images = $this->imageRepository->create($input);

            return $images;
        });
        
        if($success){
            return $this->sendResponse($success->toArray(), 'Image saved successfully');
        }else{
            return $this->sendError(array(), 'Une erreur est survenue.');
        }
    }

    /**
     * Display the specified Image.
     * GET|HEAD /images/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Image $image */
        $image = $this->imageRepository->find($id);

        if (empty($image)) {
            return $this->sendError(array(),'Image not found');
        }

        return $this->sendResponse($image->toArray(), 'Image retrieved successfully');
    }

    /**
     * Update the specified Image in storage.
     * PUT/PATCH /images/{id}
     *
     * @param int $id
     * @param UpdateImageAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateImageAPIRequest $request)
    {

        /** @var Image $image */
        $image = $this->imageRepository->find($id);

        if (empty($image)) {
            return $this->sendError(array(),'Image not found');
        }

        $input = $request->all();
        $input=array_map('trim',$input);

        $success = false;
        
        $success = \DB::transaction(function() use ($id,$input,$request){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $image = $this->imageRepository->update($input, $id);

            return $image; 
        });
        
        if($success){
            return $this->sendResponse($success->toArray(), 'Image updated successfully');
        }else{
            return $this->sendError(array(), 'Une erreur est survenue.');
        }
    }

    /**
     * Remove the specified Image from storage.
     * DELETE /images/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Image $image */
        $image = $this->imageRepository->find($id);

        if (empty($image)) {
            return $this->sendError(array(),'Image not found');
        }

        $success = false;
        
        $success = \DB::transaction(function() use ($image){
            $image->delete();

            return true; 
        });
        
        if($success){
            return $this->sendResponse(array(), 'Image deleted successfully');
        }else{
            return $this->sendError(array(), 'Une erreur est survenue.');
        }

    }
}
