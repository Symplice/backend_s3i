<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAlbumphotoAPIRequest;
use App\Http\Requests\API\UpdateAlbumphotoAPIRequest;
use App\Models\Albumphoto;
use App\Repositories\AlbumphotoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Validator;

/**
 * Class AlbumphotoController
 * @package App\Http\Controllers\API
 */

class AlbumphotoAPIController extends AppBaseController
{
    /** @var  AlbumphotoRepository */
    private $albumphotoRepository;

    public function __construct(AlbumphotoRepository $albumphotoRepo)
    {
        $this->albumphotoRepository = $albumphotoRepo;
    }

    /**
     * Display a listing of the Albumphoto.
     * GET|HEAD /albumphotos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $albumphotos = $this->albumphotoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($albumphotos->toArray(), 'Albumphotos retrieved successfully');
    }

    /**
     * Store a newly created Albumphoto in storage.
     * POST /albumphotos
     *
     * @param CreateAlbumphotoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAlbumphotoAPIRequest $request)
    {
        $input = $request->all();
        $input=array_map('trim',$input);

        $success = false;
        
        $success = \DB::transaction(function() use ($request,$input){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $albumphotos = $this->albumphotoRepository->create($input);

            return $albumphotos;
        });
        
        if($success){
            return $this->sendResponse($success->toArray(), 'Albumphoto saved successfully');
        }else{
            return $this->sendError(array(), 'Une erreur est survenue.');
        }
    }

    /**
     * Display the specified Albumphoto.
     * GET|HEAD /albumphotos/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Albumphoto $albumphoto */
        $albumphoto = $this->albumphotoRepository->find($id);

        if (empty($albumphoto)) {
            return $this->sendError(array(),'Albumphoto not found');
        }

        return $this->sendResponse($albumphoto->toArray(), 'Albumphoto retrieved successfully');
    }

    /**
     * Update the specified Albumphoto in storage.
     * PUT/PATCH /albumphotos/{id}
     *
     * @param int $id
     * @param UpdateAlbumphotoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAlbumphotoAPIRequest $request)
    {

        /** @var Albumphoto $albumphoto */
        $albumphoto = $this->albumphotoRepository->find($id);

        if (empty($albumphoto)) {
            return $this->sendError(array(),'Albumphoto not found');
        }

        $input = $request->all();
        $input=array_map('trim',$input);

        $success = false;
        
        $success = \DB::transaction(function() use ($id,$input,$request){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $albumphoto = $this->albumphotoRepository->update($input, $id);

            return $albumphoto; 
        });
        
        if($success){
            return $this->sendResponse($success->toArray(), 'Albumphoto updated successfully');
        }else{
            return $this->sendError(array(), 'Une erreur est survenue.');
        }
    }

    /**
     * Remove the specified Albumphoto from storage.
     * DELETE /albumphotos/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Albumphoto $albumphoto */
        $albumphoto = $this->albumphotoRepository->find($id);

        if (empty($albumphoto)) {
            return $this->sendError(array(),'Albumphoto not found');
        }

        $success = false;
        
        $success = \DB::transaction(function() use ($albumphoto){
            $albumphoto->delete();

            return true; 
        });
        
        if($success){
            return $this->sendResponse(array(), 'Albumphoto deleted successfully');
        }else{
            return $this->sendError(array(), 'Une erreur est survenue.');
        }

    }
}
