<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAccommodationAPIRequest;
use App\Http\Requests\API\UpdateAccommodationAPIRequest;
use App\Models\Accommodation;
use App\Repositories\AccommodationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Validator;

/**
 * Class AccommodationController
 * @package App\Http\Controllers\API
 */

class AccommodationAPIController extends AppBaseController
{
    /** @var  AccommodationRepository */
    private $accommodationRepository;

    public function __construct(AccommodationRepository $accommodationRepo)
    {
        $this->accommodationRepository = $accommodationRepo;
    }

    /**
     * Display a listing of the Accommodation.
     * GET|HEAD /accommodations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $accommodations = $this->accommodationRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($accommodations->toArray(), 'Accommodations retrieved successfully');
    }

    /**
     * Store a newly created Accommodation in storage.
     * POST /accommodations
     *
     * @param CreateAccommodationAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAccommodationAPIRequest $request)
    {
        $input = $request->all();
        $input=array_map('trim',$input);

        $success = false;
        
        $success = \DB::transaction(function() use ($request,$input){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $accommodations = $this->accommodationRepository->create($input);

            return $accommodations;
        });
        
        if($success){
            return $this->sendResponse($success->toArray(), 'Accommodation saved successfully');
        }else{
            return $this->sendError(array(), 'Une erreur est survenue.');
        }
    }

    /**
     * Display the specified Accommodation.
     * GET|HEAD /accommodations/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Accommodation $accommodation */
        $accommodation = $this->accommodationRepository->find($id);

        if (empty($accommodation)) {
            return $this->sendError(array(),'Accommodation not found');
        }

        return $this->sendResponse($accommodation->toArray(), 'Accommodation retrieved successfully');
    }

    /**
     * Update the specified Accommodation in storage.
     * PUT/PATCH /accommodations/{id}
     *
     * @param int $id
     * @param UpdateAccommodationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAccommodationAPIRequest $request)
    {

        /** @var Accommodation $accommodation */
        $accommodation = $this->accommodationRepository->find($id);

        if (empty($accommodation)) {
            return $this->sendError(array(),'Accommodation not found');
        }

        $input = $request->all();
        $input=array_map('trim',$input);

        $success = false;
        
        $success = \DB::transaction(function() use ($id,$input,$request){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $accommodation = $this->accommodationRepository->update($input, $id);

            return $accommodation; 
        });
        
        if($success){
            return $this->sendResponse($success->toArray(), 'Accommodation updated successfully');
        }else{
            return $this->sendError(array(), 'Une erreur est survenue.');
        }
    }

    /**
     * Remove the specified Accommodation from storage.
     * DELETE /accommodations/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Accommodation $accommodation */
        $accommodation = $this->accommodationRepository->find($id);

        if (empty($accommodation)) {
            return $this->sendError(array(),'Accommodation not found');
        }

        $success = false;
        
        $success = \DB::transaction(function() use ($accommodation){
            $accommodation->delete();

            return true; 
        });
        
        if($success){
            return $this->sendResponse(array(), 'Accommodation deleted successfully');
        }else{
            return $this->sendError(array(), 'Une erreur est survenue.');
        }

    }
}
