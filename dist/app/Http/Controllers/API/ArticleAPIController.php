<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateArticleAPIRequest;
use App\Http\Requests\API\UpdateArticleAPIRequest;
use App\Models\Article;
use App\Repositories\ArticleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Validator;

/**
 * Class ArticleController
 * @package App\Http\Controllers\API
 */

class ArticleAPIController extends AppBaseController
{
    /** @var  ArticleRepository */
    private $articleRepository;

    public function __construct(ArticleRepository $articleRepo)
    {
        $this->articleRepository = $articleRepo;
    }

    /**
     * Display a listing of the Article.
     * GET|HEAD /articles
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $articles = $this->articleRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($articles->toArray(), 'Articles retrieved successfully');
    }

    /**
     * Store a newly created Article in storage.
     * POST /articles
     *
     * @param CreateArticleAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateArticleAPIRequest $request)
    {
        $input = $request->all();
        $input=array_map('trim',$input);

        $success = false;
        
        $success = \DB::transaction(function() use ($request,$input){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $articles = $this->articleRepository->create($input);

            return $articles;
        });
        
        if($success){
            return $this->sendResponse($success->toArray(), 'Article saved successfully');
        }else{
            return $this->sendError(array(), 'Une erreur est survenue.');
        }
    }

    /**
     * Display the specified Article.
     * GET|HEAD /articles/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Article $article */
        $article = $this->articleRepository->find($id);

        if (empty($article)) {
            return $this->sendError(array(),'Article not found');
        }

        return $this->sendResponse($article->toArray(), 'Article retrieved successfully');
    }

    /**
     * Update the specified Article in storage.
     * PUT/PATCH /articles/{id}
     *
     * @param int $id
     * @param UpdateArticleAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateArticleAPIRequest $request)
    {

        /** @var Article $article */
        $article = $this->articleRepository->find($id);

        if (empty($article)) {
            return $this->sendError(array(),'Article not found');
        }

        $input = $request->all();
        $input=array_map('trim',$input);

        $success = false;
        
        $success = \DB::transaction(function() use ($id,$input,$request){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $article = $this->articleRepository->update($input, $id);

            return $article; 
        });
        
        if($success){
            return $this->sendResponse($success->toArray(), 'Article updated successfully');
        }else{
            return $this->sendError(array(), 'Une erreur est survenue.');
        }
    }

    /**
     * Remove the specified Article from storage.
     * DELETE /articles/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Article $article */
        $article = $this->articleRepository->find($id);

        if (empty($article)) {
            return $this->sendError(array(),'Article not found');
        }

        $success = false;
        
        $success = \DB::transaction(function() use ($article){
            $article->delete();

            return true; 
        });
        
        if($success){
            return $this->sendResponse(array(), 'Article deleted successfully');
        }else{
            return $this->sendError(array(), 'Une erreur est survenue.');
        }

    }
}
