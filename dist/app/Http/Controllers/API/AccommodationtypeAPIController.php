<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAccommodationtypeAPIRequest;
use App\Http\Requests\API\UpdateAccommodationtypeAPIRequest;
use App\Models\Accommodationtype;
use App\Repositories\AccommodationtypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;
use Validator;

/**
 * Class AccommodationtypeController
 * @package App\Http\Controllers\API
 */

class AccommodationtypeAPIController extends AppBaseController
{
    /** @var  AccommodationtypeRepository */
    private $accommodationtypeRepository;

    public function __construct(AccommodationtypeRepository $accommodationtypeRepo)
    {
        $this->accommodationtypeRepository = $accommodationtypeRepo;
    }

    /**
     * Display a listing of the Accommodationtype.
     * GET|HEAD /accommodationtypes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $accommodationtypes = $this->accommodationtypeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($accommodationtypes->toArray(), 'Accommodationtypes retrieved successfully');
    }

    /**
     * Store a newly created Accommodationtype in storage.
     * POST /accommodationtypes
     *
     * @param CreateAccommodationtypeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAccommodationtypeAPIRequest $request)
    {
        $input = $request->all();
        $input=array_map('trim',$input);

        $validation = Validator::make($input, Accommodationtype::$rules);

        if ($validation->passes())
        {
            $success = false;
            
            $success = \DB::transaction(function() use ($request,$input){

                foreach ($input as $k => $v) {
                    if (!$request->filled($k)) {
                        unset($input[$k]);
                    }
                }

                $accommodationtypes = $this->accommodationtypeRepository->create($input);

                return $accommodationtypes;
            });
            
            if($success){
                return $this->sendResponse($success->toArray(), 'Accommodationtype saved successfully');
            }else{
                return $this->sendError(array(), 'Une erreur est survenue.');
            }

        }

        return $this->sendError($validation->errors()->all(),'Erreur de validation.',422);
    }

    /**
     * Display the specified Accommodationtype.
     * GET|HEAD /accommodationtypes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Accommodationtype $accommodationtype */
        $accommodationtype = $this->accommodationtypeRepository->find($id);

        if (empty($accommodationtype)) {
            return $this->sendError(array(),'Accommodationtype not found');
        }

        return $this->sendResponse($accommodationtype->toArray(), 'Accommodationtype retrieved successfully');
    }

    /**
     * Update the specified Accommodationtype in storage.
     * PUT/PATCH /accommodationtypes/{id}
     *
     * @param int $id
     * @param UpdateAccommodationtypeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAccommodationtypeAPIRequest $request)
    {

        /** @var Accommodationtype $accommodationtype */
        $accommodationtype = $this->accommodationtypeRepository->find($id);

        if (empty($accommodationtype)) {
            return $this->sendError(array(),'Accommodationtype not found');
        }

        $input = $request->all();
        $input=array_map('trim',$input);

        $validation = Validator::make($input, Accommodationtype::$rules);

        if ($validation->passes())
        {
            $success = false;
            
            $success = \DB::transaction(function() use ($id,$input,$request){

                foreach ($input as $k => $v) {
                    if (!$request->filled($k)) {
                        unset($input[$k]);
                    }
                }

                $accommodationtype = $this->accommodationtypeRepository->update($input, $id);

                return $accommodationtype; 
            });
            
            if($success){
                return $this->sendResponse($success->toArray(), 'Accommodationtype updated successfully');
            }else{
                return $this->sendError(array(), 'Une erreur est survenue.');
            }
        }

        return $this->sendError($validation->errors()->all(),'Erreur de validation.',422);  
    }

    /**
     * Remove the specified Accommodationtype from storage.
     * DELETE /accommodationtypes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Accommodationtype $accommodationtype */
        $accommodationtype = $this->accommodationtypeRepository->find($id);

        if (empty($accommodationtype)) {
            return $this->sendError(array(),'Accommodationtype not found');
        }

        $success = false;
        
        $success = \DB::transaction(function() use ($accommodationtype){
            $accommodationtype->delete();

            return true; 
        });
        
        if($success){
            return $this->sendResponse(array(), 'Accommodationtype deleted successfully');
        }else{
            return $this->sendError(array(), 'Une erreur est survenue.');
        }

    }
}
