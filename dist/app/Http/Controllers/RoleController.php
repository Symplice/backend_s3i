<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Flash;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();

        return view('roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input=array_map('trim',$input);

        $validation = \Validator::make($input, Role::$rules);

        if ($validation->passes())
        {

            $success = false;
            
            $success = \DB::transaction(function() use ($request,$input){

                foreach ($input as $k => $v) {
                    if (!$request->filled($k)) {
                        unset($input[$k]);
                    }
                }

                $role = Role::create($input);

                return $role;
            });
            
            if($success){
                Flash::success('Le rôle a bien été créé.');
                return redirect(route('roles.create'));
            }else{
                Flash::error('Une erreur est survenue.');
                return redirect(route('roles.create'))
                            ->withInput();
            }
            
        }

        Flash::error('Erreur de validation.');
        return redirect(route('roles.create'))
                            ->withInput()
                            ->withErrors($validation);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::findOrFail($id);

        $permissions = Permission::all()->pluck('label','name');

        return view('roles.edit', compact('role','permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $role = Role::findOrFail($id);

        $input = $request->all();
        $input=array_map('trim',$input);

        $validation = \Validator::make($input, Role::$rules);

        if ($validation->passes())
        {

            $success = false;
            
            $success = \DB::transaction(function() use ($request,$input,$role){

                foreach ($input as $k => $v) {
                    if (!$request->filled($k)) {
                        unset($input[$k]);
                    }
                }

                $role = $role->update($input);

                return $role;
            });
            
            if($success){
                Flash::success('Le rôle a bien été modifié.');
                return redirect(route('roles.edit',['role'=>$id]));
            }else{
                Flash::error('Une erreur est survenue.');
                return redirect(route('roles.edit',['role'=>$id]))
                            ->withInput();
            }
            
        }

        Flash::error('Erreur de validation.');
        return redirect(route('roles.edit',['role'=>$id]))
                            ->withInput()
                            ->withErrors($validation);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);

        $role->delete();
        
        Flash::success('Le rôle a bien été supprimé.');
        return redirect(route('roles.index'));
    }

    /**
     * Assign Permissions to a role.
     *
     * @param \Illuminate\Http\Request
     *
     * @return \Illuminate\Http\Response
     */
    public function addPermission(Request $request)
    {
        $role = Role::findorfail($request->role_id);
        $role->givePermissionTo($request->permissions);

        return redirect()->action('RoleController@edit',['user'=>$request->role_id]);
    }

    /**
     * revoke Permission to a user.
     *
     * @param \Illuminate\Http\Request
     *
     * @return \Illuminate\Http\Response
     */
    public function revokePermission($permission, $role_id)
    {
        $role = Role::findorfail($role_id);

        $role->revokePermissionTo(str_slug($permission, '_'));

        return redirect()->action('RoleController@edit',['role'=>$role_id]);
    }
}
