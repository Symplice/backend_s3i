<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Flash;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::all();

        return view('permissions.index', compact('permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('permissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input=array_map('trim',$input);

        $validation = \Validator::make($input, Permission::$rules);

        if ($validation->passes())
        {

            $success = false;
            
            $success = \DB::transaction(function() use ($request,$input){

                foreach ($input as $k => $v) {
                    if (!$request->filled($k)) {
                        unset($input[$k]);
                    }
                }

                $permission = Permission::create($input);

                return $permission;
            });
            
            if($success){
                Flash::success('La permission a bien été créée.');

                return redirect(route('permissions.create'));
            }else{
                Flash::error('Une erreur est survenue.');

                return redirect(route('permissions.create'))->withInput();
            }
            
        }

        Flash::error('Erreur de validation.');

        return redirect(route('permissions.create'))->withInput()
                            ->withErrors($validation);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::findOrFail($id);

        return view('permissions.edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $permission = Permission::findOrFail($id);

        $input = $request->all();
        $input=array_map('trim',$input);

        $validation = \Validator::make($input, Permission::$rules);

        if ($validation->passes())
        {

            $success = false;
            
            $success = \DB::transaction(function() use ($request,$input,$permission){

                foreach ($input as $k => $v) {
                    if (!$request->filled($k)) {
                        unset($input[$k]);
                    }
                }

                $permission = $permission->update($input);

                return $permission;
            });
            
            if($success){
                Flash::success('La permission a bien été modifiée.');

                return redirect(route('permissions.edit',['permission'=>$id]));
            }else{
                Flash::error('Une erreur est survenue.');

                return redirect(route('permissions.edit',['permission'=>$id]))
                            ->withInput();
            }
            
        }

        Flash::error('Erreur de validation.');
        return redirect(route('permissions.edit',['permission'=>$id]))
                            ->withInput()
                            ->withErrors($validation);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission = Permission::findOrFail($id);

        $permission->delete();

        Flash::success('La permission a bien été supprimée.');

        return redirect(route('permissions.index'));
    }
}
