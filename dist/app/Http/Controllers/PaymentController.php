<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePaymentRequest;
use App\Http\Requests\UpdatePaymentRequest;
use App\Repositories\PaymentRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Bank;
use App\Models\Subscription;
use Illuminate\Http\Request;
use Flash;
use Response;
use Spatie\Permission\Models\Role;

class PaymentController extends AppBaseController
{
    /** @var  PaymentRepository */
    private $paymentRepository;

    public function __construct(PaymentRepository $paymentRepo)
    {
        $this->paymentRepository = $paymentRepo;
    }

    /**
     * Display a listing of the Payment.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $payments = $this->paymentRepository->all();
        return view('payments.index',compact('payments'));
    }

    /**
     * Show the form for creating a new Payment.
     *
     * @return Response
     */
    public function create()
    {
        $role = Role::findByName('customer');
        $users = $role->users;
        $banks = Bank::get();
        $subscriptions = Subscription::get();
        return view('payments.create',compact('users','banks','subscriptions'));
    }

    /**
     * Store a newly created Payment in storage.
     *
     * @param CreatePaymentRequest $request
     *
     * @return Response
     */
    public function store(CreatePaymentRequest $request)
    {
        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $payment = $this->paymentRepository->create($input);

            return $payment;
        });
        
        if($success){
            Flash::success('Payment a bien été ajouté.');

            return redirect()->action('PaymentController@create');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('PaymentController@create')->withInput();
        }
    }

    /**
     * Display the specified Payment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $payment = $this->paymentRepository->find($id);
        if (empty($payment)) {
            Flash::error('Payment not found');
            return redirect()->action('PaymentController@index');
        }
        return view('payments.show',compact('payment'));
    }

    /**
     * Show the form for editing the specified Payment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $payment = $this->paymentRepository->find($id);
        if (empty($payment)) {
            Flash::error('Payment not found');
            return redirect()->action('PaymentController@index');
        }
        return view('payments.edit',compact('payment'));
    }

    /**
     * Update the specified Payment in storage.
     *
     * @param  int              $id
     * @param UpdatePaymentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePaymentRequest $request)
    {
        $payment = $this->paymentRepository->find($id);

        if (empty($payment)) {
            Flash::error('Payment not found');

            return redirect()->action('PaymentController@create');
        }

        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input,$id){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $payment = $this->paymentRepository->update($input, $id);

            return $payment;
        });
        
        if($success){
            Flash::success('Payment a bien été modifié.');

            return redirect()->action('PaymentController@edit',['payment'=>$id]);
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('PaymentController@edit',['payment'=>$id])->withInput();
        }
    }

    /**
     * Remove the specified Payment from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $payment = $this->paymentRepository->find($id);

        if (empty($payment)) {
            Flash::error('Payment not found');

            return redirect()->action('PaymentController@index');
        }

        $success = false;
            
        $success = \DB::transaction(function() use ($id){

            $this->paymentRepository->delete($id);

            return true;
        });
        
        if($success){
            Flash::success('Payment a bien été supprimé.');

            return redirect()->action('PaymentController@index');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('PaymentController@index');
        }
    }
}
