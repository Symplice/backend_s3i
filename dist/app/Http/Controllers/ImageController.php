<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateImageRequest;
use App\Http\Requests\UpdateImageRequest;
use App\Repositories\ImageRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ImageController extends AppBaseController
{
    /** @var  ImageRepository */
    private $imageRepository;

    public function __construct(ImageRepository $imageRepo)
    {
        $this->imageRepository = $imageRepo;
    }

    /**
     * Display a listing of the Image.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $images = $this->imageRepository->all();

        return view('images.index',compact('images'));
    }

    /**
     * Show the form for creating a new Image.
     *
     * @return Response
     */
    public function create()
    {
        return view('images.create');
    }

    /**
     * Store a newly created Image in storage.
     *
     * @param CreateImageRequest $request
     *
     * @return Response
     */
    public function store(CreateImageRequest $request)
    {
        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $image = $this->imageRepository->create($input);

            return $image;
        });
        
        if($success){
            Flash::success('Image a bien été ajouté.');

            return redirect()->action('ImageController@create');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('ImageController@create')->withInput();
        }
    }

    /**
     * Display the specified Image.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $image = $this->imageRepository->find($id);

        if (empty($image)) {
            Flash::error('Image not found');

            return redirect()->action('ImageController@index');
        }

        return view('images.show',compact('image'));
    }

    /**
     * Show the form for editing the specified Image.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $image = $this->imageRepository->find($id);

        if (empty($image)) {
            Flash::error('Image not found');

            return redirect()->action('ImageController@index');
        }

        return view('images.edit',compact('image'));
    }

    /**
     * Update the specified Image in storage.
     *
     * @param  int              $id
     * @param UpdateImageRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateImageRequest $request)
    {
        $image = $this->imageRepository->find($id);

        if (empty($image)) {
            Flash::error('Image not found');

            return redirect()->action('ImageController@create');
        }

        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input,$id){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $image = $this->imageRepository->update($input, $id);

            return $image;
        });
        
        if($success){
            Flash::success('Image a bien été modifié.');

            return redirect()->action('ImageController@edit',['image'=>$id]);
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('ImageController@edit',['image'=>$id])->withInput();
        }
    }

    /**
     * Remove the specified Image from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $image = $this->imageRepository->find($id);

        if (empty($image)) {
            Flash::error('Image not found');

            return redirect()->action('ImageController@index');
        }

        $success = false;
            
        $success = \DB::transaction(function() use ($id){

            $this->imageRepository->delete($id);

            return true;
        });
        
        if($success){
            Flash::success('Image a bien été supprimé.');

            return redirect()->action('ImageController@index');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('ImageController@index');
        }
    }
}
