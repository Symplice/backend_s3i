<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProgramRequest;
use App\Http\Requests\UpdateProgramRequest;
use App\Repositories\ProgramRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ProgramController extends AppBaseController
{
    /** @var  ProgramRepository */
    private $programRepository;

    public function __construct(ProgramRepository $programRepo)
    {
        $this->programRepository = $programRepo;
    }

    /**
     * Display a listing of the Program.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $programs = $this->programRepository->all();

        return view('programs.index',compact('programs'));
    }

    /**
     * Show the form for creating a new Program.
     *
     * @return Response
     */
    public function create()
    {
        return view('programs.create');
    }

    /**
     * Store a newly created Program in storage.
     *
     * @param CreateProgramRequest $request
     *
     * @return Response
     */
    public function store(CreateProgramRequest $request)
    {
        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $program = $this->programRepository->create($input);

            return $program;
        });
        
        if($success){
            Flash::success('Program a bien été ajouté.');

            return redirect()->action('ProgramController@create');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('ProgramController@create')->withInput();
        }
    }

    /**
     * Display the specified Program.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $program = $this->programRepository->find($id);

        if (empty($program)) {
            Flash::error('Program not found');

            return redirect()->action('ProgramController@index');
        }

        return view('programs.show',compact('program'));
    }

    /**
     * Show the form for editing the specified Program.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $program = $this->programRepository->find($id);

        if (empty($program)) {
            Flash::error('Program not found');

            return redirect()->action('ProgramController@index');
        }

        return view('programs.edit',compact('program'));
    }

    /**
     * Update the specified Program in storage.
     *
     * @param  int              $id
     * @param UpdateProgramRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProgramRequest $request)
    {
        $program = $this->programRepository->find($id);

        if (empty($program)) {
            Flash::error('Program not found');

            return redirect()->action('ProgramController@create');
        }

        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input,$id){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $program = $this->programRepository->update($input, $id);

            return $program;
        });
        
        if($success){
            Flash::success('Program a bien été modifié.');

            return redirect()->action('ProgramController@edit',['program'=>$id]);
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('ProgramController@edit',['program'=>$id])->withInput();
        }
    }

    /**
     * Remove the specified Program from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $program = $this->programRepository->find($id);

        if (empty($program)) {
            Flash::error('Program not found');

            return redirect()->action('ProgramController@index');
        }

        $success = false;
            
        $success = \DB::transaction(function() use ($id){

            $this->programRepository->delete($id);

            return true;
        });
        
        if($success){
            Flash::success('Program a bien été supprimé.');

            return redirect()->action('ProgramController@index');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('ProgramController@index');
        }
    }
}
