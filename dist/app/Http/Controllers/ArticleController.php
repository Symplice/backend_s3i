<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateArticleRequest;
use App\Http\Requests\UpdateArticleRequest;
use App\Repositories\ArticleRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ArticleController extends AppBaseController
{
    /** @var  ArticleRepository */
    private $articleRepository;

    public function __construct(ArticleRepository $articleRepo)
    {
        $this->articleRepository = $articleRepo;
    }

    /**
     * Display a listing of the Article.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $articles = $this->articleRepository->all();

        return view('articles.index',compact('articles'));
    }

    /**
     * Show the form for creating a new Article.
     *
     * @return Response
     */
    public function create()
    {
        return view('articles.create');
    }

    /**
     * Store a newly created Article in storage.
     *
     * @param CreateArticleRequest $request
     *
     * @return Response
     */
    public function store(CreateArticleRequest $request)
    {
        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $article = $this->articleRepository->create($input);

            return $article;
        });
        
        if($success){
            Flash::success('Article a bien été ajouté.');

            return redirect()->action('ArticleController@create');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('ArticleController@create')->withInput();
        }
    }

    /**
     * Display the specified Article.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $article = $this->articleRepository->find($id);

        if (empty($article)) {
            Flash::error('Article not found');

            return redirect()->action('ArticleController@index');
        }

        return view('articles.show',compact('article'));
    }

    /**
     * Show the form for editing the specified Article.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $article = $this->articleRepository->find($id);

        if (empty($article)) {
            Flash::error('Article not found');

            return redirect()->action('ArticleController@index');
        }

        return view('articles.edit',compact('article'));
    }

    /**
     * Update the specified Article in storage.
     *
     * @param  int              $id
     * @param UpdateArticleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateArticleRequest $request)
    {
        $article = $this->articleRepository->find($id);

        if (empty($article)) {
            Flash::error('Article not found');

            return redirect()->action('ArticleController@create');
        }

        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input,$id){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $article = $this->articleRepository->update($input, $id);

            return $article;
        });
        
        if($success){
            Flash::success('Article a bien été modifié.');

            return redirect()->action('ArticleController@edit',['article'=>$id]);
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('ArticleController@edit',['article'=>$id])->withInput();
        }
    }

    /**
     * Remove the specified Article from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $article = $this->articleRepository->find($id);

        if (empty($article)) {
            Flash::error('Article not found');

            return redirect()->action('ArticleController@index');
        }

        $success = false;
            
        $success = \DB::transaction(function() use ($id){

            $this->articleRepository->delete($id);

            return true;
        });
        
        if($success){
            Flash::success('Article a bien été supprimé.');

            return redirect()->action('ArticleController@index');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('ArticleController@index');
        }
    }
}
