<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAccommodationRequest;
use App\Http\Requests\UpdateAccommodationRequest;
use App\Repositories\AccommodationRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class AccommodationController extends AppBaseController
{
    /** @var  AccommodationRepository */
    private $accommodationRepository;

    public function __construct(AccommodationRepository $accommodationRepo)
    {
        $this->accommodationRepository = $accommodationRepo;
    }

    /**
     * Display a listing of the Accommodation.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $accommodations = $this->accommodationRepository->all();

        return view('accommodations.index',compact('accommodations'));
    }

    /**
     * Show the form for creating a new Accommodation.
     *
     * @return Response
     */
    public function create()
    {
        return view('accommodations.create');
    }

    /**
     * Store a newly created Accommodation in storage.
     *
     * @param CreateAccommodationRequest $request
     *
     * @return Response
     */
    public function store(CreateAccommodationRequest $request)
    {
        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $accommodation = $this->accommodationRepository->create($input);

            return $accommodation;
        });
        
        if($success){
            Flash::success('Accommodation a bien été ajouté.');

            return redirect()->action('AccommodationController@create');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('AccommodationController@create')->withInput();
        }
    }

    /**
     * Display the specified Accommodation.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $accommodation = $this->accommodationRepository->find($id);

        if (empty($accommodation)) {
            Flash::error('Accommodation not found');

            return redirect()->action('AccommodationController@index');
        }

        return view('accommodations.show',compact('accommodation'));
    }

    /**
     * Show the form for editing the specified Accommodation.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $accommodation = $this->accommodationRepository->find($id);

        if (empty($accommodation)) {
            Flash::error('Accommodation not found');

            return redirect()->action('AccommodationController@index');
        }

        return view('accommodations.edit',compact('accommodation'));
    }

    /**
     * Update the specified Accommodation in storage.
     *
     * @param  int              $id
     * @param UpdateAccommodationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAccommodationRequest $request)
    {
        $accommodation = $this->accommodationRepository->find($id);

        if (empty($accommodation)) {
            Flash::error('Accommodation not found');

            return redirect()->action('AccommodationController@create');
        }

        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input,$id){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $accommodation = $this->accommodationRepository->update($input, $id);

            return $accommodation;
        });
        
        if($success){
            Flash::success('Accommodation a bien été modifié.');

            return redirect()->action('AccommodationController@edit',['accommodation'=>$id]);
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('AccommodationController@edit',['accommodation'=>$id])->withInput();
        }
    }

    /**
     * Remove the specified Accommodation from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $accommodation = $this->accommodationRepository->find($id);

        if (empty($accommodation)) {
            Flash::error('Accommodation not found');

            return redirect()->action('AccommodationController@index');
        }

        $success = false;
            
        $success = \DB::transaction(function() use ($id){

            $this->accommodationRepository->delete($id);

            return true;
        });
        
        if($success){
            Flash::success('Accommodation a bien été supprimé.');

            return redirect()->action('AccommodationController@index');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('AccommodationController@index');
        }
    }
}
