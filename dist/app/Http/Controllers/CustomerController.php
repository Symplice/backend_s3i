<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use App\Repositories\CustomerRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use Spatie\Permission\Models\Role;

class CustomerController extends AppBaseController
{
    /** @var  CustomerRepository */
    private $customerRepository;

    public function __construct(CustomerRepository $customerRepo)
    {
        $this->customerRepository = $customerRepo;
    }

    /**
     * Display a listing of the Customer.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $customers = $this->customerRepository->all();

        return view('customers.index',compact('customers'));
    }

    /**
     * Show the form for creating a new Customer.
     *
     * @return Response
     */
    public function create()
    {
        $role = Role::findByName('customer');
        $users = $role->users;
        return view('customers.create',compact('users'));
    }

    /**
     * Store a newly created Customer in storage.
     *
     * @param CreateCustomerRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomerRequest $request)
    {
        $input = $request->all();
        $success = false;
        $success = \DB::transaction(function() use ($request,$input){
            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }
            $customer = $this->customerRepository->create($input);
            return $customer;
        });
        
        if($success){
            Flash::success('Customer a bien été ajouté.');
            return redirect()->action('CustomerController@create');
        }else{
            Flash::error('Une erreur est survenue.');
            return redirect()->action('CustomerController@create')->withInput();
        }
    }

    /**
     * Display the specified Customer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect()->action('CustomerController@index');
        }

        return view('customers.show',compact('customer'));
    }

    /**
     * Show the form for editing the specified Customer.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect()->action('CustomerController@index');
        }

        return view('customers.edit',compact('customer'));
    }

    /**
     * Update the specified Customer in storage.
     *
     * @param  int              $id
     * @param UpdateCustomerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomerRequest $request)
    {
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect()->action('CustomerController@create');
        }

        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input,$id){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $customer = $this->customerRepository->update($input, $id);

            return $customer;
        });
        
        if($success){
            Flash::success('Customer a bien été modifié.');

            return redirect()->action('CustomerController@edit',['customer'=>$id]);
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('CustomerController@edit',['customer'=>$id])->withInput();
        }
    }

    /**
     * Remove the specified Customer from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $customer = $this->customerRepository->find($id);

        if (empty($customer)) {
            Flash::error('Customer not found');

            return redirect()->action('CustomerController@index');
        }

        $success = false;
            
        $success = \DB::transaction(function() use ($id){

            $this->customerRepository->delete($id);

            return true;
        });
        
        if($success){
            Flash::success('Customer a bien été supprimé.');

            return redirect()->action('CustomerController@index');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('CustomerController@index');
        }
    }
}
