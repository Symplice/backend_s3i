<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAccommodationtypeRequest;
use App\Http\Requests\UpdateAccommodationtypeRequest;
use App\Repositories\AccommodationtypeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class AccommodationtypeController extends AppBaseController
{
    /** @var  AccommodationtypeRepository */
    private $accommodationtypeRepository;

    public function __construct(AccommodationtypeRepository $accommodationtypeRepo)
    {
        $this->accommodationtypeRepository = $accommodationtypeRepo;
    }

    /**
     * Display a listing of the Accommodationtype.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $accommodationtypes = $this->accommodationtypeRepository->all();

        return view('accommodationtypes.index',compact('accommodationtypes'));
    }

    /**
     * Show the form for creating a new Accommodationtype.
     *
     * @return Response
     */
    public function create()
    {
        return view('accommodationtypes.create');
    }

    /**
     * Store a newly created Accommodationtype in storage.
     *
     * @param CreateAccommodationtypeRequest $request
     *
     * @return Response
     */
    public function store(CreateAccommodationtypeRequest $request)
    {
        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $accommodationtype = $this->accommodationtypeRepository->create($input);

            return $accommodationtype;
        });
        
        if($success){
            Flash::success('Accommodationtype a bien été ajouté.');

            return redirect()->action('AccommodationtypeController@create');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('AccommodationtypeController@create')->withInput();
        }
    }

    /**
     * Display the specified Accommodationtype.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $accommodationtype = $this->accommodationtypeRepository->find($id);

        if (empty($accommodationtype)) {
            Flash::error('Accommodationtype not found');

            return redirect()->action('AccommodationtypeController@index');
        }

        return view('accommodationtypes.show',compact('accommodationtype'));
    }

    /**
     * Show the form for editing the specified Accommodationtype.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $accommodationtype = $this->accommodationtypeRepository->find($id);

        if (empty($accommodationtype)) {
            Flash::error('Accommodationtype not found');

            return redirect()->action('AccommodationtypeController@index');
        }

        return view('accommodationtypes.edit',compact('accommodationtype'));
    }

    /**
     * Update the specified Accommodationtype in storage.
     *
     * @param  int              $id
     * @param UpdateAccommodationtypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAccommodationtypeRequest $request)
    {
        $accommodationtype = $this->accommodationtypeRepository->find($id);

        if (empty($accommodationtype)) {
            Flash::error('Accommodationtype not found');

            return redirect()->action('AccommodationtypeController@create');
        }

        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input,$id){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $accommodationtype = $this->accommodationtypeRepository->update($input, $id);

            return $accommodationtype;
        });
        
        if($success){
            Flash::success('Accommodationtype a bien été modifié.');

            return redirect()->action('AccommodationtypeController@edit',['accommodationtype'=>$id]);
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('AccommodationtypeController@edit',['accommodationtype'=>$id])->withInput();
        }
    }

    /**
     * Remove the specified Accommodationtype from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $accommodationtype = $this->accommodationtypeRepository->find($id);

        if (empty($accommodationtype)) {
            Flash::error('Accommodationtype not found');

            return redirect()->action('AccommodationtypeController@index');
        }

        $success = false;
            
        $success = \DB::transaction(function() use ($id){

            $this->accommodationtypeRepository->delete($id);

            return true;
        });
        
        if($success){
            Flash::success('Accommodationtype a bien été supprimé.');

            return redirect()->action('AccommodationtypeController@index');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('AccommodationtypeController@index');
        }
    }
}
