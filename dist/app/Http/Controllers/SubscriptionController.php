<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSubscriptionRequest;
use App\Http\Requests\UpdateSubscriptionRequest;
use App\Repositories\SubscriptionRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class SubscriptionController extends AppBaseController
{
    /** @var  SubscriptionRepository */
    private $subscriptionRepository;

    public function __construct(SubscriptionRepository $subscriptionRepo)
    {
        $this->subscriptionRepository = $subscriptionRepo;
    }

    /**
     * Display a listing of the Subscription.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $subscriptions = $this->subscriptionRepository->all();

        return view('subscriptions.index',compact('subscriptions'));
    }

    /**
     * Show the form for creating a new Subscription.
     *
     * @return Response
     */
    public function create()
    {
        return view('subscriptions.create');
    }

    /**
     * Store a newly created Subscription in storage.
     *
     * @param CreateSubscriptionRequest $request
     *
     * @return Response
     */
    public function store(CreateSubscriptionRequest $request)
    {
        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $subscription = $this->subscriptionRepository->create($input);

            return $subscription;
        });
        
        if($success){
            Flash::success('Subscription a bien été ajouté.');

            return redirect()->action('SubscriptionController@create');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('SubscriptionController@create')->withInput();
        }
    }

    /**
     * Display the specified Subscription.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $subscription = $this->subscriptionRepository->find($id);

        if (empty($subscription)) {
            Flash::error('Subscription not found');

            return redirect()->action('SubscriptionController@index');
        }

        return view('subscriptions.show',compact('subscription'));
    }

    /**
     * Show the form for editing the specified Subscription.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $subscription = $this->subscriptionRepository->find($id);

        if (empty($subscription)) {
            Flash::error('Subscription not found');

            return redirect()->action('SubscriptionController@index');
        }

        return view('subscriptions.edit',compact('subscription'));
    }

    /**
     * Update the specified Subscription in storage.
     *
     * @param  int              $id
     * @param UpdateSubscriptionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSubscriptionRequest $request)
    {
        $subscription = $this->subscriptionRepository->find($id);

        if (empty($subscription)) {
            Flash::error('Subscription not found');

            return redirect()->action('SubscriptionController@create');
        }

        $input = $request->all();

        $success = false;
            
        $success = \DB::transaction(function() use ($request,$input,$id){

            foreach ($input as $k => $v) {
                if (!$request->filled($k)) {
                    unset($input[$k]);
                }
            }

            $subscription = $this->subscriptionRepository->update($input, $id);

            return $subscription;
        });
        
        if($success){
            Flash::success('Subscription a bien été modifié.');

            return redirect()->action('SubscriptionController@edit',['subscription'=>$id]);
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('SubscriptionController@edit',['subscription'=>$id])->withInput();
        }
    }

    /**
     * Remove the specified Subscription from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $subscription = $this->subscriptionRepository->find($id);

        if (empty($subscription)) {
            Flash::error('Subscription not found');

            return redirect()->action('SubscriptionController@index');
        }

        $success = false;
            
        $success = \DB::transaction(function() use ($id){

            $this->subscriptionRepository->delete($id);

            return true;
        });
        
        if($success){
            Flash::success('Subscription a bien été supprimé.');

            return redirect()->action('SubscriptionController@index');
        }else{
            Flash::error('Une erreur est survenue.');

            return redirect()->action('SubscriptionController@index');
        }
    }
}
