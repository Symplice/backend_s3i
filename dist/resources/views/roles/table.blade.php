<table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;" id="roles-table">
    <thead>
        <tr>
            <th>Libellé</th>
            <th>Role</th>
            <th>Permissions</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    @foreach($roles as $role)
        <tr>
            <td>{{$role->label}}</td>
            <td>{{$role->name}}</td>
            <td>
                @if(count($role->permissions))
                    @foreach($role->permissions as $permission)
                    <span class = 'badge badge-warning'>{{$permission->label}}</span>
                    @endforeach
                @else
                    <span class="badge badge-danger">Aucune permissions</span>
                @endif
            </td>
            <td>
                {!! Form::open(['route' => ['roles.destroy', $role->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('roles.edit', [$role->id]) !!}" class='btn btn-primary btn-xs'><i class="dripicons-document-edit"></i></a>
                    {!! Form::button('<i class="dripicons-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Etes vous sûr ?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>