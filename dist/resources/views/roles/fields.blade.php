<!-- label Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('label', 'Libellé:') !!}
	    {!! Form::text('label', null, ['class' => 'form-control']) !!}
	</div>
</div>

<!-- name Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('name', 'Rôle:') !!}
	    {!! Form::text('name', null, ['class' => 'form-control']) !!}
	</div>
</div>

<!-- Submit Field -->
<div class="col-sm-12 m-t-20">
	<div class="form-group">
	    <button type="submit" class="btn btn-primary waves-effect waves-light">Enregistrer</button>
	    <a href="{!! route('roles.index') !!}" class="btn btn-secondary waves-effect m-l-5">Retour</a>
	</div>
</div>
