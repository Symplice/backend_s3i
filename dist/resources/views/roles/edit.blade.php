@extends('layouts.app')
@section('title','Modifier un rôle')
@section('content')
<div class="row">
        <div class="col-6">
            <div class="card">
                <div class="card-body">
                    @include('flash::message')
                    @include('adminlte-templates::common.errors')
                    <h4 class="mt-0 header-title">Veuillez remplir les champs</h4>
                    <div class="clearfix"></div>
                    {!! Form::model($role, ['route' => ['roles.update', $role->id], 'method' => 'patch']) !!}
                        <div class="row">

                            @include('roles.fields')

                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Attribuer une ou des permissions</h3>
                </div>
                <div class="card-body">
                    <form method="post" action="{{url('roles/addPermission')}}">
                        {!! csrf_field() !!}
                        <input type="hidden" name = "role_id" value = "{{$role->id}}">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select name="permissions[]" class = "form-control select2 select2-show-search" multiple>
                                            @foreach($permissions as $k => $permission)
                                            <option value="{{$k}}">{{$permission}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class = 'btn btn-primary'>Attribuer</button>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <table class = 'table'>
                                <thead>
                                    <th>Permission</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                    @if($role->permissions)
                                        @foreach($role->permissions as $permission)
                                        <tr>
                                            <td>{{$permission->label}}</td>
                                            <td><a href="{{url('roles/removePermission')}}/{{str_slug($permission->name,'-')}}/{{$role->id}}" class = "btn btn-danger btn-sm"><i class="dripicons-trash" aria-hidden="true"></i></a></td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script type="text/javascript">
    $('.select2-show-search').select2({
        minimumResultsForSearch: ''
    });
</script>
@endsection