<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $article->title !!}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{!! $article->slug !!}</p>
</div>

<!-- Category Id Field -->
<div class="form-group">
    {!! Form::label('category_id', 'Category Id:') !!}
    <p>{!! $article->category_id !!}</p>
</div>

<!-- Resume Field -->
<div class="form-group">
    {!! Form::label('resume', 'Resume:') !!}
    <p>{!! $article->resume !!}</p>
</div>

<!-- Video Link Field -->
<div class="form-group">
    {!! Form::label('video_link', 'Video Link:') !!}
    <p>{!! $article->video_link !!}</p>
</div>

<!-- Body Field -->
<div class="form-group">
    {!! Form::label('body', 'Body:') !!}
    <p>{!! $article->body !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $article->user_id !!}</p>
</div>

