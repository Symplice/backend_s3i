<table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;" id="articles-table">
    <thead>
        <tr>
            <th>Title</th>
        {{-- <th>Slug</th> --}}
        <th>Category Id</th>
        <th>Resume</th>
        <th>Video Link</th>
        <th>Body</th>
        <th>User Id</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    @foreach($articles as $article)
        <tr>
            <td>{!! $article->title !!}</td>
            {{-- <td>{!! $article->slug !!}</td> --}}
            <td>{!! $article->category_id !!}</td>
            <td>{!! $article->resume !!}</td>
            <td>{!! $article->video_link !!}</td>
            <td>{!! $article->body !!}</td>
            <td>{!! $article->user_id !!}</td>
            <td>
                {!! Form::open(['route' => ['articles.destroy', $article->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('articles.show', [$article->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('articles.edit', [$article->id]) !!}" class='btn btn-primary btn-xs'><i class="dripicons-document-edit"></i></a>
                    {!! Form::button('<i class="dripicons-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Etes vous sûr ?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>