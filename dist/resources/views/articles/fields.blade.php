<!-- Title Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('title', 'Title:') !!}
	    <div>
			{!! Form::text('title', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Slug Field -->
{{-- <div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('slug', 'Slug:') !!}
	    <div>
			{!! Form::text('slug', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div> --}}

<!-- Category Id Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('category_id', 'Category Id:') !!}
	    <div>
			{!! Form::text('category_id', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Resume Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('resume', 'Resume:') !!}
	    <div>
			{!! Form::text('resume', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Video Link Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('video_link', 'Video Link:') !!}
	    <div>
			{!! Form::text('video_link', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Body Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('body', 'Body:') !!}
	    <div>
			{!! Form::text('body', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- User Id Field -->

{{-- <div class="col-sm-4">
	<div class="form-group">
		{!! Form::label('user_id', 'Selectionner un utilisateur:') !!}
	</div>
	<div class="form-group">
		<select name="user_id" class = "form-control select2 select2-show-search">
			@foreach($users as $user)
			<option value="{{$user->id}}">{{$user->name}}</option>
			@endforeach
		</select>
	</div>
</div> --}}

<!-- Submit Field -->
<div class="col-sm-12">
	<div class="form-group">
		<button type="submit" class="btn btn-primary waves-effect waves-light">Enregistrer</button>
	    <a href="{!! route('articles.index') !!}" class="btn btn-secondary waves-effect m-l-5">Retour</a>
	</div>
</div>
