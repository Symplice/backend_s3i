@extends('layouts.app')
@section('title')
    Connexion
@endsection
@section('content')
    <form class="form-horizontal m-t-20" method="post" action="{{ url('/login') }}">
        {!! csrf_field() !!}
        <div class="form-group row">
            <div class="col-12">
                <input class="form-control {{ $errors->has('email') ? ' parsley-error' : '' }}" type="email" required value="{{ old('email') }}" name="email" placeholder="Nom d'utilisateur">
                @if ($errors->has('email'))
                    <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('email') }}</li></ul>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12">
                <input class="form-control{{ $errors->has('password') ? ' parsley-error' : '' }}" name="password" type="password" required placeholder="Mot de passe">
                @if ($errors->has('password'))
                    <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('password') }}</li></ul>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" name="remember" class="custom-control-input" id="customCheck1">
                    <label class="custom-control-label" for="customCheck1">Se souvenir de moi</label>
                </div>
            </div>
        </div>
        <div class="form-group text-center row m-t-20">
            <div class="col-12">
                <button class="btn btn-danger btn-block waves-effect waves-light" type="submit">Connexion</button>
            </div>
        </div>
        <div class="form-group m-t-10 mb-0 row">
            <div class="col-sm-7 m-t-20">
                <a href="{{ url('/password/reset') }}" class="text-muted"><i class="mdi mdi-lock"></i> <small>Mot de passe oublié ?</small></a>
            </div>
            {{-- <div class="col-sm-5 m-t-20"><a href="{{ url('/register') }}" class="text-muted"><i class="mdi mdi-account-circle"></i> <small>Créer un compte ?</small></a></div> --}}
        </div>
    </form>
@endsection