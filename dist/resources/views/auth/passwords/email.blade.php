@extends('layouts.app')
@section('title')
    Réinitialisation du mot de passe
@endsection
@section('content')
    <form class="form-horizontal" method="post" action="{{ url('/password/email') }}">
        {!! csrf_field() !!}
        @if (!session('status'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Enter your <b>Email</b> and instructions will be sent to you!</div>
        @else
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> {{ session('status') }}</div>
        @endif
        <div class="form-group row">
            <div class="col-12">
                <input class="form-control {{ $errors->has('email') ? ' parsley-error' : '' }}" type="email" required value="{{ old('email') }}" placeholder="Adresse e-mail">
                @if ($errors->has('email'))
                    <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('email') }}</li></ul>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group text-center row m-t-20">
            <div class="col-12">
                <button class="btn btn-danger btn-block waves-effect waves-light" type="submit">Envoyer</button>
            </div>
        </div>
    </form>
@endsection