@extends('layouts.app')
@section('title')
    Inscription
@endsection
@section('content')
    <form class="form-horizontal m-t-20" method="post" action="{{ url('/register') }}">
        {!! csrf_field() !!}
        <div class="form-group row">
            <div class="col-12">
                <input class="form-control {{ $errors->has('name') ? ' parsley-error' : '' }}" type="text" required value="{{ old('name') }}" name="name" placeholder="Nom & prénoms">
                @if ($errors->has('name'))
                    <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('name') }}</li></ul>
                </span>
                @endif
            </div>
        </div>
        {{-- <div class="form-group row">
            <div class="col-12">
                <input class="form-control {{ $errors->has('firstname') ? ' parsley-error' : '' }}" type="text" required value="{{ old('firstname') }}" name="firstname" placeholder="Prénoms">
                @if ($errors->has('firstname'))
                    <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('firstname') }}</li></ul>
                </span>
                @endif
            </div>
        </div> --}}
        <div class="form-group row">
            <div class="col-12">
                <input class="form-control {{ $errors->has('email') ? ' parsley-error' : '' }}" type="email" required value="{{ old('email') }}" name="email" placeholder="Adresse e-mail">
                @if ($errors->has('email'))
                    <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('email') }}</li></ul>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12">
                <input class="form-control {{ $errors->has('cellphone') ? ' parsley-error' : '' }}" type="text" required value="{{ old('cellphone') }}" name="cellphone" placeholder="Contact">
                @if ($errors->has('cellphone'))
                    <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('cellphone') }}</li></ul>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12">
                <input class="form-control{{ $errors->has('password') ? ' parsley-error' : '' }}" name="password" type="password" required placeholder="Mot de passe">
                @if ($errors->has('password'))
                    <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('password') }}</li></ul>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12">
                <input class="form-control{{ $errors->has('password_confirmation') ? ' parsley-error' : '' }}" name="password_confirmation" type="password" required placeholder="Confirmer mot de passe">
                @if ($errors->has('password_confirmation'))
                    <ul class="parsley-errors-list filled"><li class="parsley-required">{{ $errors->first('password_confirmation') }}</li></ul>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <div class="col-12">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" name="terms" class="custom-control-input" id="customCheck1">
                    <label class="custom-control-label" for="customCheck1">J'accepte <a href="#" class="text-muted">les termes et conditions</a></label>
                </div>
            </div>
        </div>
        <div class="form-group text-center row m-t-20">
            <div class="col-12">
                <button class="btn btn-danger btn-block waves-effect waves-light" type="submit">Inscription</button>
            </div>
        </div>
        <div class="form-group m-t-10 mb-0 row">
            <div class="col-sm-5 m-t-20"><a href="{{ url('/login') }}" class="text-muted"><i class="mdi mdi-account-circle"></i> <small>J'ai déjà un compte</small></a></div>
        </div>
    </form>
@endsection