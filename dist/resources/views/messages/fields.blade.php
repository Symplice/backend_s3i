<!-- Title Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('title', 'Title:') !!}
	    <div>
			{!! Form::text('title', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Body Field -->
<div class="col-sm-12">
	<div class="form-group">
	    {!! Form::label('body', 'Body:') !!}
	    <div>
			{!! Form::textarea('body', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Receiver Id Field -->
{{-- <div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('receiver_id', 'Receiver Id:') !!}
	    <div>
			{!! Form::number('receiver_id', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div> --}}

<div class="col-sm-4">
	<div class="form-group">
		{!! Form::label('receiver_id', 'Selectionner un client:') !!}
	</div>
	<div class="form-group">
		<select name="receiver_id" class = "form-control select2 select2-show-search">
			@foreach($users as $user)
			<option value="{{$user->id}}">{{$user->customer->firstname .' '.$user->customer->lastname}}</option>
			@endforeach
		</select>
	</div>
</div>

<!-- Submit Field -->
<div class="col-sm-12">
	<div class="form-group">
		<button type="submit" class="btn btn-primary waves-effect waves-light">Enregistrer</button>
	    <a href="{!! route('messages.index') !!}" class="btn btn-secondary waves-effect m-l-5">Retour</a>
	</div>
</div>
