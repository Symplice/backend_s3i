<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $message->title !!}</p>
</div>

<!-- Body Field -->
<div class="form-group">
    {!! Form::label('body', 'Body:') !!}
    <p>{!! $message->body !!}</p>
</div>

<!-- Receiver Id Field -->
<div class="form-group">
    {!! Form::label('receiver_id', 'Receiver Id:') !!}
    <p>{!! $message->receiver_id !!}</p>
</div>

