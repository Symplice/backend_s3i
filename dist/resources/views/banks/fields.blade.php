<!-- Name Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('name', 'Name:') !!}
	    <div>
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Slug Field -->
{{-- <div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('slug', 'Slug:') !!}
	    <div>
			{!! Form::text('slug', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div> --}}

<!-- Rib Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('rib', 'Rib:') !!}
	    <div>
			{!! Form::text('rib', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Description Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('description', 'Description:') !!}
	    <div>
			{!! Form::text('description', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Submit Field -->
<div class="col-sm-12">
	<div class="form-group">
		<button type="submit" class="btn btn-primary waves-effect waves-light">Enregistrer</button>
	    <a href="{!! route('banks.index') !!}" class="btn btn-secondary waves-effect m-l-5">Retour</a>
	</div>
</div>
