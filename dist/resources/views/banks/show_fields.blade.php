<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $bank->name !!}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{!! $bank->slug !!}</p>
</div>

<!-- Rib Field -->
<div class="form-group">
    {!! Form::label('rib', 'Rib:') !!}
    <p>{!! $bank->rib !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $bank->description !!}</p>
</div>

