<table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;" id="banks-table">
    <thead>
        <tr>
            <th>Banque</th>
        {{-- <th>Slug</th> --}}
        <th>Rib</th>
        <th>Description</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    @foreach($banks as $bank)
        <tr>
            <td>{!! $bank->name !!}</td>
            {{-- <td>{!! $bank->slug !!}</td> --}}
            <td>{!! $bank->rib !!}</td>
            <td>{!! $bank->description !!}</td>
            <td>
                {!! Form::open(['route' => ['banks.destroy', $bank->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('banks.show', [$bank->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('banks.edit', [$bank->id]) !!}" class='btn btn-primary btn-xs'><i class="dripicons-document-edit"></i></a>
                    {!! Form::button('<i class="dripicons-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Etes vous sûr ?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>