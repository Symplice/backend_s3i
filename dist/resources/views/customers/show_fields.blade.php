<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $customer->user_id !!}</p>
</div>

<!-- Account Number Field -->
<div class="form-group">
    {!! Form::label('account_number', 'Account Number:') !!}
    <p>{!! $customer->account_number !!}</p>
</div>

<!-- Civility Field -->
<div class="form-group">
    {!! Form::label('civility', 'Civility:') !!}
    <p>{!! $customer->civility !!}</p>
</div>

<!-- Photo Field -->
<div class="form-group">
    {!! Form::label('photo', 'Photo:') !!}
    <p>{!! $customer->photo !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $customer->name !!}</p>
</div>

<!-- Firstname Field -->
<div class="form-group">
    {!! Form::label('firstname', 'Firstname:') !!}
    <p>{!! $customer->firstname !!}</p>
</div>

<!-- Phone Number Field -->
<div class="form-group">
    {!! Form::label('phone_number', 'Phone Number:') !!}
    <p>{!! $customer->phone_number !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $customer->email !!}</p>
</div>

<!-- Rib Field -->
<div class="form-group">
    {!! Form::label('rib', 'Rib:') !!}
    <p>{!! $customer->rib !!}</p>
</div>

<!-- Document Field -->
<div class="form-group">
    {!! Form::label('document', 'Document:') !!}
    <p>{!! $customer->document !!}</p>
</div>

<!-- Document Reference Field -->
<div class="form-group">
    {!! Form::label('document_reference', 'Document Reference:') !!}
    <p>{!! $customer->document_reference !!}</p>
</div>

<!-- Nationality Field -->
<div class="form-group">
    {!! Form::label('nationality', 'Nationality:') !!}
    <p>{!! $customer->nationality !!}</p>
</div>

<!-- Birthdate Field -->
<div class="form-group">
    {!! Form::label('birthdate', 'Birthdate:') !!}
    <p>{!! $customer->birthdate !!}</p>
</div>

<!-- Birth Place Field -->
<div class="form-group">
    {!! Form::label('birth_place', 'Birth Place:') !!}
    <p>{!! $customer->birth_place !!}</p>
</div>

<!-- Occupation Field -->
<div class="form-group">
    {!! Form::label('occupation', 'Occupation:') !!}
    <p>{!! $customer->occupation !!}</p>
</div>

<!-- Employer Field -->
<div class="form-group">
    {!! Form::label('employer', 'Employer:') !!}
    <p>{!! $customer->employer !!}</p>
</div>

<!-- Residency Field -->
<div class="form-group">
    {!! Form::label('residency', 'Residency:') !!}
    <p>{!! $customer->residency !!}</p>
</div>

