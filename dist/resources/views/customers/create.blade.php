@extends('layouts.app')
@section('title','Ajouter un client')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @include('flash::message')
                    @include('adminlte-templates::common.errors')
                    <h4 class="mt-0 header-title">Veuillez remplir les champs</h4>
                    <div class="clearfix"></div>
                    {!! Form::open(['route' => 'customers.store']) !!}
                        <div class="row">

                            @include('customers.fields')

                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
@endsection
