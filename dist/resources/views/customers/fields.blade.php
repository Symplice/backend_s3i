<!-- User Id Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('user_id', 'Selectionner un utilisateur:') !!}
	<div class="form-group">
		<select name="user_id" class = "form-control select2 select2-show-search" multiple>
			@foreach($users as $user)
			<option value="{{$user}}">{{$user->name}}</option>
			@endforeach
		</select>
	</div>
</div>

<!-- Account Number Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('account_number', 'Account Number:') !!}
	    <div>
			{!! Form::text('account_number', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Civility Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('civility', 'Civility:') !!}
	    <div>
			{!! Form::text('civility', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Photo Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('photo', 'Photo:') !!}
	    <div>
			{!! Form::text('photo', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Name Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('name', 'Name:') !!}
	    <div>
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Firstname Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('firstname', 'Firstname:') !!}
	    <div>
			{!! Form::text('firstname', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Phone Number Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('phone_number', 'Phone Number:') !!}
	    <div>
			{!! Form::text('phone_number', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Email Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('email', 'Email:') !!}
	    <div>
			{!! Form::email('email', null, ['class' => 'form-control','placeholder'=>'Entrer le mail']) !!}
	    </div>
	</div>
</div>

<!-- Rib Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('rib', 'Rib:') !!}
	    <div>
			{!! Form::text('rib', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Document Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('document', 'Document:') !!}
	    <div>
			{!! Form::text('document', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Document Reference Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('document_reference', 'Document Reference:') !!}
	    <div>
			{!! Form::text('document_reference', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Nationality Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('nationality', 'Nationality:') !!}
	    <div>
			{!! Form::text('nationality', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Birthdate Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('birthdate', 'Birthdate:') !!}
	    <div>
			<div class="input-group">
			    {!! Form::text('birthdate', null, ['class' => 'form-control datepicker','placeholder'=>'dd/mm/yyyy']) !!}
			    <div class="input-group-append bg-custom b-0"><span class="input-group-text"><i class="mdi mdi-calendar"></i></span></div>
			</div>
	    </div>
	</div>
</div>

<!-- Birth Place Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('birth_place', 'Birth Place:') !!}
	    <div>
			{!! Form::text('birth_place', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Occupation Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('occupation', 'Occupation:') !!}
	    <div>
			{!! Form::text('occupation', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Employer Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('employer', 'Employer:') !!}
	    <div>
			{!! Form::text('employer', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Residency Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('residency', 'Residency:') !!}
	    <div>
			{!! Form::text('residency', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Submit Field -->
<div class="col-sm-12">
	<div class="form-group">
		<button type="submit" class="btn btn-primary waves-effect waves-light">Enregistrer</button>
	    <a href="{!! route('customers.index') !!}" class="btn btn-secondary waves-effect m-l-5">Retour</a>
	</div>
</div>
