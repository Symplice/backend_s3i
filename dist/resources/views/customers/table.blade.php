<table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;" id="customers-table">
    <thead>
        <tr>
            <th>User Id</th>
        <th>Account Number</th>
        <th>Civility</th>
        <th>Photo</th>
        <th>Name</th>
        <th>Firstname</th>
        <th>Phone Number</th>
        <th>Email</th>
        <th>Rib</th>
        <th>Document</th>
        <th>Document Reference</th>
        <th>Nationality</th>
        <th>Birthdate</th>
        <th>Birth Place</th>
        <th>Occupation</th>
        <th>Employer</th>
        <th>Residency</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    @foreach($customers as $customer)
        <tr>
            <td>{!! $customer->user_id !!}</td>
            <td>{!! $customer->account_number !!}</td>
            <td>{!! $customer->civility !!}</td>
            <td>{!! $customer->photo !!}</td>
            <td>{!! $customer->name !!}</td>
            <td>{!! $customer->firstname !!}</td>
            <td>{!! $customer->phone_number !!}</td>
            <td>{!! $customer->email !!}</td>
            <td>{!! $customer->rib !!}</td>
            <td>{!! $customer->document !!}</td>
            <td>{!! $customer->document_reference !!}</td>
            <td>{!! $customer->nationality !!}</td>
            <td>{!! $customer->birthdate !!}</td>
            <td>{!! $customer->birth_place !!}</td>
            <td>{!! $customer->occupation !!}</td>
            <td>{!! $customer->employer !!}</td>
            <td>{!! $customer->residency !!}</td>
            <td>
                {!! Form::open(['route' => ['customers.destroy', $customer->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('customers.show', [$customer->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('customers.edit', [$customer->id]) !!}" class='btn btn-primary btn-xs'><i class="dripicons-document-edit"></i></a>
                    {!! Form::button('<i class="dripicons-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Etes vous sûr ?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>