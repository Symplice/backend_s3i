<!-- User Id Field -->
<div class="col-sm-4">
	<div class="form-group">
		{!! Form::label('user_id', 'Selectionner un client:') !!}
	</div>
	<div class="form-group">
		<select name="user_id" class = "form-control select2 select2-show-search">
			@foreach($users as $user)
			<option value="{{$user->id}}">{{$user->customer->firstname}}</option>
			@endforeach
		</select>
	</div>
</div>

<!-- Customer Id Field -->
{{-- <div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('customer_id', 'Customer Id:') !!}
	    <div>
			{!! Form::number('customer_id', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div> --}}

<!-- Program Id Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('program_id', 'Program Id:') !!}
	    <div>
			{!! Form::number('program_id', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Accommodation Id Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('accommodation_id', 'Accommodation Id:') !!}
	    <div>
			{!! Form::number('accommodation_id', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Application Fees Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('application_fees', 'Application Fees:') !!}
	    <div>
			{!! Form::number('application_fees', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Initial Contribution Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('initial_contribution', 'Initial Contribution:') !!}
	    <div>
			{!! Form::text('initial_contribution', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Delivery Formula Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('delivery_formula', 'Delivery Formula:') !!}
	    <div>
			{!! Form::text('delivery_formula', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Status Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('status', 'Status:') !!}
	    <div>
			{!! Form::text('status', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Submit Field -->
<div class="col-sm-12">
	<div class="form-group">
		<button type="submit" class="btn btn-primary waves-effect waves-light">Enregistrer</button>
	    <a href="{!! route('subscriptions.index') !!}" class="btn btn-secondary waves-effect m-l-5">Retour</a>
	</div>
</div>
