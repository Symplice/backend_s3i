@extends('layouts.app')
@section('title','Modifier un Subscription')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @include('flash::message')
                    @include('adminlte-templates::common.errors')
                    <h4 class="mt-0 header-title">Veuillez remplir les champs</h4>
                    <div class="clearfix"></div>
                    {!! Form::model($subscription, ['route' => ['subscriptions.update', $subscription->id], 'method' => 'patch']) !!}
                        <div class="row">

                            @include('subscriptions.fields')

                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
@endsection
