<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $subscription->user_id !!}</p>
</div>

<!-- Customer Id Field -->
<div class="form-group">
    {!! Form::label('customer_id', 'Customer Id:') !!}
    <p>{!! $subscription->customer_id !!}</p>
</div>

<!-- Program Id Field -->
<div class="form-group">
    {!! Form::label('program_id', 'Program Id:') !!}
    <p>{!! $subscription->program_id !!}</p>
</div>

<!-- Accommodation Id Field -->
<div class="form-group">
    {!! Form::label('accommodation_id', 'Accommodation Id:') !!}
    <p>{!! $subscription->accommodation_id !!}</p>
</div>

<!-- Application Fees Field -->
<div class="form-group">
    {!! Form::label('application_fees', 'Application Fees:') !!}
    <p>{!! $subscription->application_fees !!}</p>
</div>

<!-- Initial Contribution Field -->
<div class="form-group">
    {!! Form::label('initial_contribution', 'Initial Contribution:') !!}
    <p>{!! $subscription->initial_contribution !!}</p>
</div>

<!-- Delivery Formula Field -->
<div class="form-group">
    {!! Form::label('delivery_formula', 'Delivery Formula:') !!}
    <p>{!! $subscription->delivery_formula !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $subscription->status !!}</p>
</div>

