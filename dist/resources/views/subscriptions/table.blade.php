<table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;" id="subscriptions-table">
    <thead>
        <tr>
            <th>User Id</th>
        <th>Customer Id</th>
        <th>Program Id</th>
        <th>Accommodation Id</th>
        <th>Application Fees</th>
        <th>Initial Contribution</th>
        <th>Delivery Formula</th>
        <th>Status</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    @foreach($subscriptions as $subscription)
        <tr>
            <td>{!! $subscription->user_id !!}</td>
            <td>{!! $subscription->customer_id !!}</td>
            <td>{!! $subscription->program_id !!}</td>
            <td>{!! $subscription->accommodation_id !!}</td>
            <td>{!! $subscription->application_fees !!}</td>
            <td>{!! $subscription->initial_contribution !!}</td>
            <td>{!! $subscription->delivery_formula !!}</td>
            <td>{!! $subscription->status !!}</td>
            <td>
                {!! Form::open(['route' => ['subscriptions.destroy', $subscription->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('subscriptions.show', [$subscription->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('subscriptions.edit', [$subscription->id]) !!}" class='btn btn-primary btn-xs'><i class="dripicons-document-edit"></i></a>
                    {!! Form::button('<i class="dripicons-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Etes vous sûr ?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>