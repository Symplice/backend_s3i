<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $accommodation->name !!}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{!! $accommodation->slug !!}</p>
</div>

<!-- Program Id Field -->
<div class="form-group">
    {!! Form::label('program_id', 'Program Id:') !!}
    <p>{!! $accommodation->program_id !!}</p>
</div>

<!-- Accommodation Type Id Field -->
<div class="form-group">
    {!! Form::label('accommodation_type_id', 'Accommodation Type Id:') !!}
    <p>{!! $accommodation->accommodation_type_id !!}</p>
</div>

<!-- Rooms Number Field -->
<div class="form-group">
    {!! Form::label('rooms_number', 'Rooms Number:') !!}
    <p>{!! $accommodation->rooms_number !!}</p>
</div>

<!-- Bathroom Field -->
<div class="form-group">
    {!! Form::label('bathroom', 'Bathroom:') !!}
    <p>{!! $accommodation->bathroom !!}</p>
</div>

<!-- Lot Field -->
<div class="form-group">
    {!! Form::label('lot', 'Lot:') !!}
    <p>{!! $accommodation->lot !!}</p>
</div>

<!-- Ilot Field -->
<div class="form-group">
    {!! Form::label('ilot', 'Ilot:') !!}
    <p>{!! $accommodation->ilot !!}</p>
</div>

<!-- Parking Place Field -->
<div class="form-group">
    {!! Form::label('parking_place', 'Parking Place:') !!}
    <p>{!! $accommodation->parking_place !!}</p>
</div>

<!-- Terrace Field -->
<div class="form-group">
    {!! Form::label('terrace', 'Terrace:') !!}
    <p>{!! $accommodation->terrace !!}</p>
</div>

<!-- Dining Room Field -->
<div class="form-group">
    {!! Form::label('dining_room', 'Dining Room:') !!}
    <p>{!! $accommodation->dining_room !!}</p>
</div>

<!-- Living Room Field -->
<div class="form-group">
    {!! Form::label('living_room', 'Living Room:') !!}
    <p>{!! $accommodation->living_room !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{!! $accommodation->price !!}</p>
</div>

<!-- Kitchen Field -->
<div class="form-group">
    {!! Form::label('kitchen', 'Kitchen:') !!}
    <p>{!! $accommodation->kitchen !!}</p>
</div>

