<!-- Name Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('name', 'Name:') !!}
	    <div>
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Slug Field -->
{{-- <div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('slug', 'Slug:') !!}
	    <div>
			{!! Form::text('slug', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div> --}}

<!-- Program Id Field -->
<div class="col-sm-4">
	<div class="form-group">
		{!! Form::label('program_id', 'Selectionner un programme:') !!}
	</div>
	<div class="form-group">
		<select name="user_id" class = "form-control select2 select2-show-search">
			@foreach($programs as $prog)
				<option value="{{$prog->id}}">{{$prog->name}}</option>
			@endforeach
		</select>
	</div>
</div>

<!-- Accommodation Type Id Field -->
<div class="col-sm-4">
	<div class="form-group">
		{!! Form::label('accommodation_type_id', 'Selectionner un type de logement:') !!}
	</div>
	<div class="form-group">
		<select name="accommodation_type_id" class = "form-control select2 select2-show-search">
			@foreach($accommodation_types as $acct)
			<option value="{{$acct->id}}">{{$acct->name }}</option>
			@endforeach
		</select>
	</div>
</div>

<!-- Rooms Number Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('rooms_number', 'Rooms Number:') !!}
	    <div>
			{!! Form::number('rooms_number', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Bathroom Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('bathroom', 'Bathroom:') !!}
	    <div>
			{!! Form::number('bathroom', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Lot Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('lot', 'Lot:') !!}
	    <div>
			{!! Form::text('lot', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Ilot Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('ilot', 'Ilot:') !!}
	    <div>
			{!! Form::text('ilot', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Parking Place Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('parking_place', 'Parking Place:') !!}
	    <div>
			{!! Form::number('parking_place', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Terrace Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('terrace', 'Terrace:') !!}
	    <div>
			{!! Form::number('terrace', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Dining Room Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('dining_room', 'Dining Room:') !!}
	    <div>
			{!! Form::number('dining_room', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Living Room Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('living_room', 'Living Room:') !!}
	    <div>
			{!! Form::number('living_room', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Price Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('price', 'Price:') !!}
	    <div>
			{!! Form::number('price', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Kitchen Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('kitchen', 'Kitchen:') !!}
	    <div>
			{!! Form::number('kitchen', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Submit Field -->
<div class="col-sm-12">
	<div class="form-group">
		<button type="submit" class="btn btn-primary waves-effect waves-light">Enregistrer</button>
	    <a href="{!! route('accommodations.index') !!}" class="btn btn-secondary waves-effect m-l-5">Retour</a>
	</div>
</div>
