<table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;" id="accommodations-table">
    <thead>
        <tr>
        <th>Nom</th>
        <th>Programme</th>
        <th>Type  de logement</th>
        <th>Nombre de chambre</th>
        <th>Bathroom</th>
        <th>Lot</th>
        <th>Ilot</th>
        <th>Place de Parking</th>
        <th>Terrace</th>
        <th>Salle à manger</th>
        <th>Chambre de sejour</th>
        <th>Price</th>
        <th>Cuisine</th>
        <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    @foreach($accommodations as $accommodation)
        <tr>
            <td>{!! $accommodation->name !!}</td>
            {{-- <td>{!! $accommodation->slug !!}</td> --}}
            <td>{!! $accommodation->program->name !!}</td>
            <td>{!! $accommodation->accommodation_type->name!!}</td>
            <td>{!! $accommodation->rooms_number !!}</td>
            <td>{!! $accommodation->bathroom !!}</td>
            <td>{!! $accommodation->lot !!}</td>
            <td>{!! $accommodation->ilot !!}</td>
            <td>{!! $accommodation->parking_place !!}</td>
            <td>{!! $accommodation->terrace !!}</td>
            <td>{!! $accommodation->dining_room !!}</td>
            <td>{!! $accommodation->living_room !!}</td>
            <td>{!! $accommodation->price !!}</td>
            <td>{!! $accommodation->kitchen !!}</td>
            <td>
                {!! Form::open(['route' => ['accommodations.destroy', $accommodation->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('accommodations.show', [$accommodation->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('accommodations.edit', [$accommodation->id]) !!}" class='btn btn-primary btn-xs'><i class="dripicons-document-edit"></i></a>
                    {!! Form::button('<i class="dripicons-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Etes vous sûr ?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>