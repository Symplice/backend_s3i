<!-- Name Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('name', 'Nom:') !!}
	    <div>
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Accommodation Id Field -->

<div class="col-sm-4">
	<div class="form-group">
		{!! Form::label('accommodation_id', 'Selectionner un logement:') !!}
	</div>
	<div class="form-group">
		<select name="accommodation_id" class = "form-control select2 select2-show-search">
			@foreach($accommodations as $acc)
			<option value="{{$acc->id}}">{{$acc->name}}</option>
			@endforeach
		</select>
	</div>
</div>

<!-- Submit Field -->
<div class="col-sm-12">
	<div class="form-group">
		<button type="submit" class="btn btn-primary waves-effect waves-light">Enregistrer</button>
	    <a href="{!! route('albumphotos.index') !!}" class="btn btn-secondary waves-effect m-l-5">Retour</a>
	</div>
</div>
