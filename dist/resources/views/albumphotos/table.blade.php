<table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;" id="albumphotos-table">
    <thead>
        <tr>
            <th>Name</th>
        <th>Accommodation Id</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    @foreach($albumphotos as $albumphoto)
        <tr>
            <td>{!! $albumphoto->name !!}</td>
            <td>{!! $albumphoto->accommodation_id !!}</td>
            <td>
                {!! Form::open(['route' => ['albumphotos.destroy', $albumphoto->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('albumphotos.show', [$albumphoto->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('albumphotos.edit', [$albumphoto->id]) !!}" class='btn btn-primary btn-xs'><i class="dripicons-document-edit"></i></a>
                    {!! Form::button('<i class="dripicons-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Etes vous sûr ?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>