<!-- Album Photo Id Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('album_photo_id', 'Album Photo Id:') !!}
	    <div>
			{!! Form::number('album_photo_id', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Name Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('name', 'Name:') !!}
	    <div>
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Submit Field -->
<div class="col-sm-12">
	<div class="form-group">
		<button type="submit" class="btn btn-primary waves-effect waves-light">Enregistrer</button>
	    <a href="{!! route('images.index') !!}" class="btn btn-secondary waves-effect m-l-5">Retour</a>
	</div>
</div>
