<!-- Album Photo Id Field -->
<div class="form-group">
    {!! Form::label('album_photo_id', 'Album Photo Id:') !!}
    <p>{!! $image->album_photo_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $image->name !!}</p>
</div>

