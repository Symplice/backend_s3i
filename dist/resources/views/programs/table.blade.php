<table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;" id="programs-table">
    <thead>
        <tr>
            <th>Name</th>
        <th>Slug</th>
        <th>Description</th>
        <th>City</th>
        <th>Town</th>
        <th>Accommodation Number</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    @foreach($programs as $program)
        <tr>
            <td>{!! $program->name !!}</td>
            <td>{!! $program->slug !!}</td>
            <td>{!! $program->description !!}</td>
            <td>{!! $program->city !!}</td>
            <td>{!! $program->town !!}</td>
            <td>{!! $program->accommodation_number !!}</td>
            <td>
                {!! Form::open(['route' => ['programs.destroy', $program->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('programs.show', [$program->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('programs.edit', [$program->id]) !!}" class='btn btn-primary btn-xs'><i class="dripicons-document-edit"></i></a>
                    {!! Form::button('<i class="dripicons-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Etes vous sûr ?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>