<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $program->name !!}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{!! $program->slug !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $program->description !!}</p>
</div>

<!-- City Field -->
<div class="form-group">
    {!! Form::label('city', 'City:') !!}
    <p>{!! $program->city !!}</p>
</div>

<!-- Town Field -->
<div class="form-group">
    {!! Form::label('town', 'Town:') !!}
    <p>{!! $program->town !!}</p>
</div>

<!-- Accommodation Number Field -->
<div class="form-group">
    {!! Form::label('accommodation_number', 'Accommodation Number:') !!}
    <p>{!! $program->accommodation_number !!}</p>
</div>

