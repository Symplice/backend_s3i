<!-- Name Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('name', 'Name:') !!}
	    <div>
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Slug Field -->
{{-- <div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('slug', 'Slug:') !!}
	    <div>
			{!! Form::text('slug', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div> --}}

<!-- Description Field -->
<div class="col-sm-12">
	<div class="form-group">
	    {!! Form::label('description', 'Description:') !!}
	    <div>
			{!! Form::textarea('description', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- City Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('city', 'City:') !!}
	    <div>
			{!! Form::text('city', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Town Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('town', 'Town:') !!}
	    <div>
			{!! Form::text('town', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Accommodation Number Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('accommodation_number', 'Accommodation Number:') !!}
	    <div>
			{!! Form::number('accommodation_number', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Submit Field -->
<div class="col-sm-12">
	<div class="form-group">
		<button type="submit" class="btn btn-primary waves-effect waves-light">Enregistrer</button>
	    <a href="{!! route('programs.index') !!}" class="btn btn-secondary waves-effect m-l-5">Retour</a>
	</div>
</div>
