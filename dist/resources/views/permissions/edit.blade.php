@extends('layouts.app')
@section('title','Modifier une permission')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @include('flash::message')
                    @include('adminlte-templates::common.errors')
                    <h4 class="mt-0 header-title">Veuillez remplir les champs</h4>
                    <div class="clearfix"></div>
                    {!! Form::model($permission, ['route' => ['permissions.update', $user->id], 'method' => 'patch']) !!}
                        <div class="row">

                            @include('permissions.fields')

                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $('.select2-show-search').select2({
        minimumResultsForSearch: ''
    });
</script>
@endsection