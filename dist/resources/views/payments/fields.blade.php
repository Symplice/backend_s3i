<!-- User Id Field -->
<div class="col-sm-4">
	<div class="form-group">
		{!! Form::label('user_id', 'Selectionner un client:') !!}
	</div>
	<div class="form-group">
		<select name="user_id" class = "form-control select2 select2-show-search">
			@foreach($users as $user)
			<option value="{{$user->id}}">{{$user->customer}}</option>
			@endforeach
		</select>
	</div>
</div>

<!-- Date Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('date', 'Date:') !!}
	    <div>
			<div class="input-group">
			    {!! Form::text('date', null, ['class' => 'form-control datepicker','placeholder'=>'dd/mm/yyyy']) !!}
			    <div class="input-group-append bg-custom b-0"><span class="input-group-text"><i class="mdi mdi-calendar"></i></span></div>
			</div>
	    </div>
	</div>
</div>

<!-- Amount Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('amount', 'Montant:') !!}
	    <div>
			{!! Form::number('amount', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Accommodation Id Field -->
<div class="col-sm-4">
	<div class="form-group">
		{!! Form::label('subscription_id', 'Selectionner une souscription:') !!}
	</div>
	<div class="form-group">
		<select name="subscription_id" class = "form-control select2 select2-show-search">
			@foreach($subscriptions as $subsc)
			<option value="{{$subsc->id}}">{{$subsc->customer->account_number}}</option>
			@endforeach
		</select>
	</div>
</div>

<!-- Method Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('method', 'Methode de paiement:') !!}  
		<div class="form-group">
			<select name="method" class = "form-control select2 select2-show-search">
				<option value="CASH">VERSEMENT LIQUIDE</option>
				<option value="BANK_TRANSFER">VIR. BANQUE</option>
				<option value="CHECK">CHEQUE</option>
			</select>
		</div>
	</div>
</div>

<!-- Bank Id Field -->
<div class="col-sm-4">
	<div class="form-group">
		{!! Form::label('subscription_id', 'Selectionner la banque beneficiaire:') !!}
	</div>
	<div class="form-group">
		<select name="subscription_id" class = "form-control select2 select2-show-search">
			@foreach($banks as $bank)
			<option value="{{$bank->id}}">{{$bank->name}}</option>
			@endforeach
		</select>
	</div>
</div>

<!-- Next Payment Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('next_payment', 'Echéance prochain:') !!}
	    <div>
			<div class="input-group">
			    {!! Form::text('next_payment', null, ['class' => 'form-control datepicker','placeholder'=>'dd/mm/yyyy']) !!}
			    <div class="input-group-append bg-custom b-0"><span class="input-group-text"><i class="mdi mdi-calendar"></i></span></div>
			</div>
	    </div>
	</div>
</div>

<!-- Left To Pay Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('left_to_pay', 'Reste à payer:') !!}
	    <div>
			{!! Form::number('left_to_pay', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Submit Field -->
<div class="col-sm-12">
	<div class="form-group">
		<button type="submit" class="btn btn-primary waves-effect waves-light">Enregistrer</button>
	    <a href="{!! route('payments.index') !!}" class="btn btn-secondary waves-effect m-l-5">Retour</a>
	</div>
</div>
