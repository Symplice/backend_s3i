<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $payment->user_id !!}</p>
</div>

<!-- Date Field -->
<div class="form-group">
    {!! Form::label('date', 'Date:') !!}
    <p>{!! $payment->date !!}</p>
</div>

<!-- Amount Field -->
<div class="form-group">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{!! $payment->amount !!}</p>
</div>

<!-- Accommodation Id Field -->
<div class="form-group">
    {!! Form::label('accommodation_id', 'Accommodation Id:') !!}
    <p>{!! $payment->accommodation_id !!}</p>
</div>

<!-- Method Field -->
<div class="form-group">
    {!! Form::label('method', 'Method:') !!}
    <p>{!! $payment->method !!}</p>
</div>

<!-- Bank Id Field -->
<div class="form-group">
    {!! Form::label('bank_id', 'Bank Id:') !!}
    <p>{!! $payment->bank_id !!}</p>
</div>

<!-- Next Payment Field -->
<div class="form-group">
    {!! Form::label('next_payment', 'Next Payment:') !!}
    <p>{!! $payment->next_payment !!}</p>
</div>

<!-- Left To Pay Field -->
<div class="form-group">
    {!! Form::label('left_to_pay', 'Left To Pay:') !!}
    <p>{!! $payment->left_to_pay !!}</p>
</div>

