<table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;" id="payments-table">
    <thead>
        <tr>
            <th>User Id</th>
        <th>Date</th>
        <th>Amount</th>
        <th>Accommodation Id</th>
        <th>Method</th>
        <th>Bank Id</th>
        <th>Next Payment</th>
        <th>Left To Pay</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    @foreach($payments as $payment)
        <tr>
            <td>{!! $payment->user_id !!}</td>
            <td>{!! $payment->date !!}</td>
            <td>{!! $payment->amount !!}</td>
            <td>{!! $payment->accommodation_id !!}</td>
            <td>{!! $payment->method !!}</td>
            <td>{!! $payment->bank_id !!}</td>
            <td>{!! $payment->next_payment !!}</td>
            <td>{!! $payment->left_to_pay !!}</td>
            <td>
                {!! Form::open(['route' => ['payments.destroy', $payment->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('payments.show', [$payment->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('payments.edit', [$payment->id]) !!}" class='btn btn-primary btn-xs'><i class="dripicons-document-edit"></i></a>
                    {!! Form::button('<i class="dripicons-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Etes vous sûr ?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>