<!-- User Id Field -->
<div class="col-sm-4">
	<div class="form-group">
		{!! Form::label('user_id', 'Selectionner un client:') !!}
	</div>
	<div class="form-group">
		<select name="user_id" class = "form-control select2 select2-show-search">
			@foreach($users as $user)
			<option value="{{$user->id}}">{{$user->customer->firstname .' '.$user->customer->lastname}}</option>
			@endforeach
		</select>
	</div>
</div>

<!-- Libelle Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('libelle', 'Libelle:') !!}
	    <div>
			{!! Form::text('libelle', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Status Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('status', 'Status:') !!}
	    <div>
			{!! Form::text('status', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Submit Field -->
<div class="col-sm-12">
	<div class="form-group">
		<button type="submit" class="btn btn-primary waves-effect waves-light">Enregistrer</button>
	    <a href="{!! route('subscriptionfolders.index') !!}" class="btn btn-secondary waves-effect m-l-5">Retour</a>
	</div>
</div>
