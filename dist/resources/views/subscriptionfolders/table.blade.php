<table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;" id="subscriptionfolders-table">
    <thead>
        <tr>
            <th>User Id</th>
        <th>Libelle</th>
        <th>Status</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    @foreach($subscriptionfolders as $subscriptionfolder)
        <tr>
            <td>{!! $subscriptionfolder->user_id !!}</td>
            <td>{!! $subscriptionfolder->libelle !!}</td>
            <td>{!! $subscriptionfolder->status !!}</td>
            <td>
                {!! Form::open(['route' => ['subscriptionfolders.destroy', $subscriptionfolder->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('subscriptionfolders.show', [$subscriptionfolder->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('subscriptionfolders.edit', [$subscriptionfolder->id]) !!}" class='btn btn-primary btn-xs'><i class="dripicons-document-edit"></i></a>
                    {!! Form::button('<i class="dripicons-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Etes vous sûr ?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>