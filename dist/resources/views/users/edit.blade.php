@extends('layouts.app')
@section('title','Modifier un utilisateur')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @include('flash::message')
                    @include('adminlte-templates::common.errors')
                    <h4 class="mt-0 header-title">Veuillez remplir les champs</h4>
                    <div class="clearfix"></div>
                    {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'patch']) !!}
                        <div class="row">

                            @include('users.fields')

                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <!-- end col -->
        <div class="col-sm-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Attribuer un rôle</h3>
                </div>
                <div class="card-body">
                    <form method="post" action="{{url('users/addRole')}}">
                        {!! csrf_field() !!}
                        <input type="hidden" name = "user_id" value = "{{$user->id}}">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select name="role_name" id="" class = "form-control">
                                            @foreach($roles as $k => $role)
                                            <option value="{{$k}}">{{$role}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class = 'btn btn-primary'>Attribuer</button>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <table class = 'table'>
                                <thead>
                                    <th>Role</th>
                                    <th>Action</th>
                                </thead>
                                <tbody>
                                    @if($userRoles)
                                        @foreach($userRoles as $role)
                                        <tr>
                                            <td>{{$role->label}}</td>
                                            <td><a href="{{url('users/removeRole')}}/{{str_slug($role->name,'-')}}/{{$user->id}}" class = "btn btn-danger btn-sm"><i class="dripicons-trash" aria-hidden="true"></i></a></td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
