<!-- Lastname Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('name', 'Nom d\'utilisateur :') !!}
	    <div>
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Firstname Field -->

<!-- Email Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('email', 'E-mail:') !!}
	    <div>
			{!! Form::email('email', null, ['class' => 'form-control','placeholder'=>'Entrer le mail']) !!}
	    </div>
	</div>
</div>

<!-- Password Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('password', 'Mot de passe:') !!}
	    <div>
			{!! Form::password('password', ['class' => 'form-control','placeholder'=>'Entrer le mot de passe']) !!}
	    </div>
	</div>
</div>


<!-- Role Field -->
<div class="col-sm-4">
	<div class="form-group">
		{!! Form::label('roles[]', 'Attribuer un role') !!}
		<select name="roles[]" class = "form-control select2 select2-show-search">
			@foreach($roles as $role)
				<option value="{{$role->name}}">{{$role->label}}</option>
			@endforeach
		</select>
	</div>
</div>

<!-- Submit Field -->
<div class="col-sm-12 m-t-20">
	<div class="form-group">
	    <button type="submit" class="btn btn-primary waves-effect waves-light">Enregistrer</button>
	    <a href="{!! route('users.index') !!}" class="btn btn-secondary waves-effect m-l-5">Retour</a>
	</div>
</div>