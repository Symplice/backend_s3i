@extends('layouts.app')
@section('title','Tous les utilisateurs')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @include('flash::message')
                    @include('adminlte-templates::common.errors')
                    <div class="row m-b-20">
                        <div class="col-sm-6">
                            <h4 class="header-title">Utilisateurs</h4>
                        </div>
                        <div class="col-sm-6">
                               <a class="btn btn-primary float-right" href="{!! route('users.create') !!}"><i class="dripicons-plus"></i> Ajouter</a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    @include('users.table')
                    <div class="text-center">
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
@endsection

