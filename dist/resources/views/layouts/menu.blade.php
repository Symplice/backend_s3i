<li class="menu-title">Menu principal</li>
<li>
    <a href="{{ url('/home') }}" class="waves-effect">
        <i class="dripicons-device-desktop"></i><span>Tableau de bord</span>
    </a>
</li>
@role('supadmin')
<li class="has_sub{{ Request::is('permissions*') ? ' active' : '' }}">
    <a href="javascript:void(0);" class="waves-effect{{ Request::is('permissions*') ? ' active' : '' }}"><i class="dripicons-jewel"></i> <span>Administration</span><span class="float-right"><i class="mdi mdi-chevron-right"></i></span></a>
    <ul class="list-unstyled">
        <li class="{{ Request::is('permissions') ? ' active' : '' }}">
            <a href="{!! action('PermissionController@index') !!}">Permissions</a>
        </li>
        <li class="{{ Request::is('roles') ? ' active' : '' }}">
            <a href="{!! action('RoleController@index') !!}">Rôles</a>
        </li>
        <li class="{{ Request::is('users') ? ' active' : '' }}">
            <a href="{!! action('UserController@index') !!}">Utilisateurs</a>
        </li>
        <li class="{{ Request::is('banks') ? ' active' : '' }}">
        	<a href="{!! action('BankController@index') !!}">Banques</a>
        </li>
    </ul>
</li>
@endrole

<li class="has_sub{{ Request::is('programs*') ? ' active' : '' }}">
	<a href="javascript:void(0);" class="waves-effect{{ Request::is('programs*') ? ' active' : '' }}"><i class="dripicons-jewel"></i> <span>Opérations </span><span class="float-right"><i class="mdi mdi-chevron-right"></i></span></a>
    <ul class="list-unstyled">
        <li class="{{ Request::is('programs') ? ' active' : '' }}">
        	<a href="{!! action('ProgramController@index') !!}">Programmes</a>
        </li>
        <li class="{{ Request::is('accommodationtypes') ? ' active' : '' }}">
        	<a href="{!! action('AccommodationtypeController@index') !!}">Type de logements</a>
        </li>
        <li class="{{ Request::is('accommodations') ? ' active' : '' }}">
        	<a href="{!! action('AccommodationController@index') !!}">Logements</a>
        </li>
        <li class="{{ Request::is('albumphotos') ? ' active' : '' }}">
        	<a href="{!! action('AlbumphotoController@index') !!}">Album photos</a>
        </li>
    </ul>
</li>

<li class="has_sub{{ Request::is('articles*') ? ' active' : '' }}">
	<a href="javascript:void(0);" class="waves-effect{{ Request::is('articles*') ? ' active' : '' }}"><i class="dripicons-jewel"></i> <span>Articles </span><span class="float-right"><i class="mdi mdi-chevron-right"></i></span></a>
    <ul class="list-unstyled">
        <li class="{{ Request::is('articles') ? ' active' : '' }}">
        	<a href="{!! action('ArticleController@index') !!}">Articles</a>
        </li>
        <li class="{{ Request::is('categories') ? ' active' : '' }}">
        	<a href="{!! action('CategoryController@index') !!}">Categories d'articles</a>
        </li>
    </ul>
</li>

<li class="has_sub{{ Request::is('customers*') ? ' active' : '' }}">
	<a href="javascript:void(0);" class="waves-effect{{ Request::is('customers*') ? ' active' : '' }}"><i class="dripicons-jewel"></i> <span>Offres </span><span class="float-right"><i class="mdi mdi-chevron-right"></i></span></a>
    <ul class="list-unstyled">
        <li class="{{ Request::is('subscriptions') ? ' active' : '' }}">
            <a href="{!! action('SubscriptionController@index') !!}">Souscriptions</a>
        </li>
        <li class="{{ Request::is('customers') ? ' active' : '' }}">
        	<a href="{!! action('CustomerController@index') !!}">Clients</a>
        </li>
        <li class="{{ Request::is('subscriptionfolders') ? ' active' : '' }}">
        	<a href="{!! action('SubscriptionfolderController@index') !!}">Dossiers Clients</a>
        </li>
        <li class="{{ Request::is('payments') ? ' active' : '' }}">
        	<a href="{!! action('PaymentController@index') !!}">Paiements</a>
        </li>
        <li class="{{ Request::is('messages') ? ' active' : '' }}">
        	<a href="{!! action('MessageController@index') !!}">Messages</a>
        </li>
    </ul>
</li>