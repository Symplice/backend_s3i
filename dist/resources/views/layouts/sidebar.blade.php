<div class="left side-menu">
    <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect"><i class="ion-close"></i></button>
    <!-- LOGO -->
    <div class="topbar-left">
        <div class="text-center bg-logo">
            {{-- <a href="{{action('HomeController@index')}}" class="logo"><i class="mdi mdi-bowling text-success"></i> Digimusic+</a> --}}
            <a href="{{action('HomeController@index')}}" class="logo"><img src="{{ asset('assets/images/logo.png') }}" height="24" alt="logo"></a></div>
    </div>
    <div class="sidebar-user"><img src="{{ asset('assets/images/users/avatar-6.jpg') }}" alt="user" class="rounded-circle img-thumbnail mb-1">
        <h6 class="">{{ Auth::user()->lastname.' '.Auth::user()->firstname }}</h6>
        <p class="online-icon text-dark"><i class="mdi mdi-record text-success"></i>En ligne</p>
    </div>
    <div class="sidebar-inner slimscrollleft">
        <div id="sidebar-menu">
            <ul>
                @include('layouts.menu')
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
    <!-- end sidebarinner -->
</div>