@extends('layouts.app')
@section('title','Ajouter une Categorie')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @include('flash::message')
                    @include('adminlte-templates::common.errors')
                    <h4 class="mt-0 header-title">Veuillez remplir les champs</h4>
                    <div class="clearfix"></div>
                    {!! Form::open(['route' => 'categories.store']) !!}
                        <div class="row">

                            @include('categories.fields')

                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <!-- end col -->
    </div>
@endsection
