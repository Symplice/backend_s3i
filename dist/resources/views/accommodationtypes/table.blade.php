<table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;" id="accommodationtypes-table">
    <thead>
        <tr>
        <th>Nom</th>
        <th>Description</th>
        <th>Dimension total</th>
        <th>Dimension utile</th>
        <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    @foreach($accommodationtypes as $accommodationtype)
        <tr>
            <td>{!! $accommodationtype->name !!}</td>
            <td>{!! $accommodationtype->description !!}</td>
            <td>{!! $accommodationtype->land_space !!}</td>
            <td>{!! $accommodationtype->built_area !!}</td>
            <td>
                {!! Form::open(['route' => ['accommodationtypes.destroy', $accommodationtype->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('accommodationtypes.show', [$accommodationtype->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('accommodationtypes.edit', [$accommodationtype->id]) !!}" class='btn btn-primary btn-xs'><i class="dripicons-document-edit"></i></a>
                    {!! Form::button('<i class="dripicons-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Etes vous sûr ?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>