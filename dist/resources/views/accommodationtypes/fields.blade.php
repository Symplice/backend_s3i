<!-- Name Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('name', 'Nom:') !!}
	    <div>
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Description Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('description', 'Description:') !!}
	    <div>
			{!! Form::text('description', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Land Space Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('land_space', 'Dimension totale:') !!}
	    <div>
			{!! Form::number('land_space', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Built Area Field -->
<div class="col-sm-4">
	<div class="form-group">
	    {!! Form::label('built_area', 'Espace utile:') !!}
	    <div>
			{!! Form::number('built_area', null, ['class' => 'form-control']) !!}
	    </div>
	</div>
</div>

<!-- Submit Field -->
<div class="col-sm-12">
	<div class="form-group">
		<button type="submit" class="btn btn-primary waves-effect waves-light">Enregistrer</button>
	    <a href="{!! route('accommodationtypes.index') !!}" class="btn btn-secondary waves-effect m-l-5">Retour</a>
	</div>
</div>
