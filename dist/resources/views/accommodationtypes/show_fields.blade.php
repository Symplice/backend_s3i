<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $accommodationtype->name !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $accommodationtype->description !!}</p>
</div>

<!-- Land Space Field -->
<div class="form-group">
    {!! Form::label('land_space', 'Land Space:') !!}
    <p>{!! $accommodationtype->land_space !!}</p>
</div>

<!-- Built Area Field -->
<div class="form-group">
    {!! Form::label('built_area', 'Built Area:') !!}
    <p>{!! $accommodationtype->built_area !!}</p>
</div>

